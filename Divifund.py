#!/usr/bin/env python
#
#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


import sys
import os
import string
import gobject
import gtk
import gtk.glade
import gettext
import webbrowser

from gtk import TRUE, FALSE

# Debug Messages Value
# Set to TRUE to turn on debug printing
# Set to FALSE to hide all but critical messages for end users
debugMsgs=TRUE

# OS Detection Code based on path handling, also sets
# directory/path delimiters in m_pDelim (use instead
# of slashes in paths passed through buildPath) - RP 20040603



    
def findPrefix():
    cwd=os.getcwd()
    intPrefix=cwd
    if debugMsgs:
        print('RP: CWD='+cwd)
    lCwd=len(cwd)
    if lCwd==0:
        print('RP:  This is bad.  Working directory not found, cannot generate prefix.  May die unexpectedly, fix this!')
        intPrefix='D:\\Program Files'
        valid=FALSE
    else:
        if debugMsgs:
            print('RP: CWD Test 1: ' + string.upper(cwd[lCwd-8:]))
            print('RP: CWD Test 2: ' + string.upper(cwd[lCwd-9:lCwd-1]))
        if string.upper(cwd[lCwd-8:])=='DIVIFUND':
            intPrefix=cwd[:lCwd-9]
        if string.upper(cwd[lCwd-9:lCwd-1])=='DIVIFUND':
            intPrefix=cwd[:lCwd-10]
        valid=TRUE
    if debugMsgs:
        print('RP: Working Directory: '+intPrefix)
    return intPrefix


            
sPREFIX=findPrefix()

libPath = ''
print libPath
sys.path.append(libPath)
import DivifundClass
m_Divifund = DivifundClass.Divifund(sPREFIX)
m_Divifund.main()

	   

