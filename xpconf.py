# xpconf Unifed Configuration Module for Python (Cross-Platform)
# Front end running on Neoconf beta middle layer and backends
# Copyright 2004, Ryan Pavlik
# NO WARRANTY
import sys
import os
import string
import xptools
import cs_middle

# Set to 1 to display debug messages
confDebug = 1

global m_OS
m_homeDir = os.path.expanduser
m_OS=xptools.XP_Detect()

#
# The Main Class
#
# Properties:
# appKey : string : application root key name, no spaces.  Used to build conf paths
#
# Methods:
# readValue (key as string) : returns value : Reads a setting value
# setValue (key as string, value as string) : writes value : Writes a setting value, without regard to curretn value
#
# Current schemes supported, in order of preference:
# GConf: Gnome Configuration.  Usually present on Linux or GNOME workstations
# WinReg: Windows Registry.  Not really completed, but framed in.  For M$ Win32
# FSConfTree: File-system Configuration Tree.  Fallback cross platform method.
#
#
class settings:
    # Set Application Key (name, no spaces) for use across all platforms
    global appKey
    

    def __init__(self, appName):
        self.appKey=appName
        
                
        #
        # Now Using Neoconf Middle Layer
        #
        
        #
        # Initialize Neoconf Middle Layer
        #
        self.m_Middle=cs_middle.midSettings()
        
        
        
        
        
    #
    # Method: settings.readValue (key as string)
    # Returns: value as string
    # Returns: None if key is not present or blank
    # Read setting value.  Equivalent to .get_string on GConf
    #
    def readValue(self, key):
        global cwd
        if confDebug:
            print 'xpconf: readValue(' + key +')'
        #
        # Neoconf Middle Layer
        # 
        
        sKey = self.appKey+'/'+key
        self.m_value = self.m_Middle.readValue(sKey)
        if confDebug :
            print 'xpconf: key= ',sKey,' Value=',self.m_value
        return self.m_value
        
    #
    # Method: settings.setVaule (key as string, value as string)
    # Write setting value.  Equivalent to .set_string on GConf
    #
    def setValue(self, key, value):
        global cwd
        if confDebug:
            print 'xpconf: setValue(' + key + ','+ value +')'
        #
        # Neoconf Middle Layer
        # 
        
        skey = self.appKey+'/'+key
        self.m_Middle.setValue(skey,value)
        
        return
            
