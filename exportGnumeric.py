#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


import sys
import os
import string

#
# Some useful code that should be available in all modules
#
from utilTools import dollarToFloat

class ExportGnumeric:
    def __init__(self):
        self.m_sHead = """<?xml version="1.0" encoding="UTF-8"?>
<gmr:Workbook xmlns:gmr="http://www.gnumeric.org/v10.dtd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.gnumeric.org/v8.xsd">
  <gmr:Attributes>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_horizontal_scrollbar</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_vertical_scrollbar</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::show_notebook_tabs</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::do_auto_completion</gmr:name>
      <gmr:value>TRUE</gmr:value>
    </gmr:Attribute>
    <gmr:Attribute>
      <gmr:type>4</gmr:type>
      <gmr:name>WorkbookView::is_protected</gmr:name>
      <gmr:value>FALSE</gmr:value>
    </gmr:Attribute>
  </gmr:Attributes>
  <gmr:Summary>
    <gmr:Item>
      <gmr:name>application</gmr:name>
      <gmr:val-string>gnumeric</gmr:val-string>
    </gmr:Item>
    <gmr:Item>
      <gmr:name>author</gmr:name>
      <gmr:val-string>Martin Sevior</gmr:val-string>
    </gmr:Item>
  </gmr:Summary>
  <gmr:SheetNameIndex>
    <gmr:SheetName>Sheet1</gmr:SheetName>
  </gmr:SheetNameIndex>
  <gmr:Geometry Width="1012" Height="623"/>
  <gmr:Sheets>
    <gmr:Sheet DisplayFormulas="false" HideZero="false" HideGrid="false" HideColHeader="false" HideRowHeader="false" DisplayOutlines="true" OutlineSymbolsBelow="true" OutlineSymbolsRight="true">
      <gmr:Name>Sheet1</gmr:Name>
      <gmr:MaxCol>%MaxCols%</gmr:MaxCol>
      <gmr:MaxRow>%MaxRows%</gmr:MaxRow>
      <gmr:Zoom>1.000000</gmr:Zoom>
      <gmr:PrintInformation>
        <gmr:Margins>
          <gmr:top Points="28.35" PrefUnit="cm"/>
          <gmr:bottom Points="28.35" PrefUnit="cm"/>
          <gmr:left Points="56.69" PrefUnit="Pt"/>
          <gmr:right Points="56.69" PrefUnit="Pt"/>
          <gmr:header Points="85.04" PrefUnit="Pt"/>
          <gmr:footer Points="85.04" PrefUnit="Pt"/>
        </gmr:Margins>
        <gmr:Scale type="percentage" percentage="100"/>
        <gmr:vcenter value="0"/>
        <gmr:hcenter value="0"/>
        <gmr:grid value="0"/>
        <gmr:even_if_only_styles value="0"/>
        <gmr:monochrome value="0"/>
        <gmr:draft value="0"/>
        <gmr:titles value="0"/>
        <gmr:repeat_top value=""/>
        <gmr:repeat_left value=""/>
        <gmr:order>r_then_d</gmr:order>
        <gmr:orientation>landscape</gmr:orientation>
        <gmr:Header Left="" Middle="&amp;[TAB]" Right=""/>
        <gmr:Footer Left="" Middle="Page &amp;[PAGE]" Right=""/>
        <gmr:paper>A4</gmr:paper>
      </gmr:PrintInformation>
      <gmr:Styles>
        <gmr:StyleRegion startCol="0" startRow="0" endCol="255" endRow="65535">
          <gmr:Style HAlign="1" VAlign="2" WrapText="0" ShrinkToFit="0" Rotation="0" Shade="0" Indent="0" Locked="1" Hidden="0" Fore="0:0:0" Back="FFFF:FFFF:FFFF" PatternColor="0:0:0" Format="General">
            <gmr:Font Unit="10" Bold="0" Italic="0" Underline="0" StrikeThrough="0">Sans</gmr:Font>
            <gmr:StyleBorder>
              <gmr:Top Style="0"/>
              <gmr:Bottom Style="0"/>
              <gmr:Left Style="0"/>
              <gmr:Right Style="0"/>
              <gmr:Diagonal Style="0"/>
              <gmr:Rev-Diagonal Style="0"/>
            </gmr:StyleBorder>
          </gmr:Style>
        </gmr:StyleRegion>
      </gmr:Styles>
"""
        self.m_sFoot = """     <gmr:SheetLayout TopLeft="A1"/>
      <gmr:Solver TargetCol="-1" TargetRow="-1" ProblemType="1" Inputs="" MaxTime="0" MaxIter="0" NonNeg="1" Discr="0" AutoScale="0" ShowIter="0" AnswerR="0" SensitivityR="0" LimitsR="0" PerformR="0" ProgramR="0"/>
      <gmr:Scenarios/>
    </gmr:Sheet>
  </gmr:Sheets>
  <gmr:UIData SelectedTab="0"/>
</gmr:Workbook>
"""

    def removeAmps(self,str):
        ilen = len(str)
        j = 0
        ostr = ''
        while j< ilen :
            if str[j] == '&':
                ostr = ostr + 'and'
            else:
                ostr = ostr + str[j]
            j = j + 1
        return ostr
 
#
# Export a gtk tree to a gnumeric file
#
    def export(self,tree,sGnuFile,sHeadings):
        fGnuFileName = open(sGnuFile,'w')
        model = tree.get_model()
        iterFirst = model.get_iter_first()
        countRows = 0
        iter = iterFirst
        bstop = 1
        while bstop==1:
            iter = model.iter_next(iter)
            if iter != None:
                countRows = countRows + 1
            else:
                bstop = 0

        numCols = 0
        bStop = 1
        curCol = tree.get_column(numCols)
        if curCol == None:
            bStop =0
        if bStop == 1:
            colwidth0 = curCol.get_width()    
            numCols = numCols +1
            colWidths = [colwidth0]
            while bStop == 1:
                curCol =  tree.get_column(numCols)
                if curCol == None:
                    bStop =0
                else:
                    width = tree.get_column(numCols).get_width()
                    if width > 200 :
                        width = 200
                    colWidths.append(width)
                    numCols = numCols +1

#
# write out the boiler plate header for a simple gnumeric file
# with maxcols and maxrows set to correct values
#
        for lin in self.m_sHead:
            if lin.find('%MaxCols%') >= 0:
                str = '%d' % (numCols+1)
                lin = lin.replace('%MaxCols%',str)
            if lin.find('%MaxRows%') >= 0:
                str = '%d' % (countRows+1)
                lin = lin.replace('%MaxRows%',str)
            fGnuFileName.write(lin)

# write out number of cols

        lin = '      <gmr:Cols DefaultSizePts="48">\n'
        fGnuFileName.write(lin)
        for i in range(numCols):
            colwidth = colWidths[i]
            lin ='         <gmr:ColInfo No="%d" Unit="%f" MarginA="2" MarginB="2" HardSize="1"/> \n' % (i,colwidth*0.7)
            fGnuFileName.write(lin)
        lin = '      </gmr:Cols> \n'
        fGnuFileName.write(lin)

# Number of rows

        lin ='       <gmr:Rows DefaultSizePts="12.75"> \n'
        fGnuFileName.write(lin)
        lin = '        <gmr:RowInfo No="0" Unit="12.75" MarginA="0" MarginB="0" Count="%d"/> \n' % countRows
        fGnuFileName.write(lin)
        lin = '      </gmr:Rows> \n'
        fGnuFileName.write(lin)

# OK now write out each cell

        lin = '      <gmr:Cells> \n'
        fGnuFileName.write(lin)
        iter = iterFirst
        bstop = 1
        row = 0
        bDoHeadings = 0
        if sHeadings != None:
            bDoHeadings = 1
        while bstop ==1:
            for i in range(numCols):
                sval = model.get_value(iter,i)
                sval = string.strip(sval)
                sval = self.removeAmps(sval)
                fval = 0.0
                isnum = 0
                if bDoHeadings == 1:
                    sval = sHeadings[i]
                    lin = '        <gmr:Cell Col="%d" Row="%d" ValueType="60">%s</gmr:Cell> \n' % (i,row,sval)
                else:
                    if (len(sval)> 0) and (sval[0] == '$'):
                        fval = dollarToFloat(sval)
                        isnum = 1
                    elif (len(sval)>0) and sval.isdigit():
                        fval = string.atof(sval)
                        isnum = 1
                    if isnum == 0 and len(sval) == 0:
                        lin = '        <gmr:Cell Col="%d" Row="%d" ValueType="60"> </gmr:Cell> \n' % (i,row)
                    elif isnum == 0 and len(sval) > 0:
                        lin = '        <gmr:Cell Col="%d" Row="%d" ValueType="60">%s</gmr:Cell> \n' % (i,row,sval)
                    else:
                        lin = '        <gmr:Cell Col="%d" Row="%d" ValueType="40">%f</gmr:Cell> \n' % (i,row,fval)
                fGnuFileName.write(lin)
            if bDoHeadings == 1:
                iter = iterFirst
            else:
                iter = model.iter_next(iter)
            bDoHeadings = 0
            if iter != None:
                row = row + 1
            else:
                bstop = 0

        lin = '      </gmr:Cells>\n'
        fGnuFileName.write(lin)
    
# write out the boiler plater footer for a simple gnumeric file
#
        for lin in self.m_sFoot:
            fGnuFileName.write(lin)
        fGnuFileName.close()
#
# OK now start up a gnumeric process with this file
#
        lin = 'gnumeric '+sGnuFile+' &'
        os.system(lin)
        return
