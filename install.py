#!/usr/bin/env python
import sys
import os
import readline
import string

if len(sys.argv) > 1:
    sPrefix = sys.argv[1]
else:
    sPrefix = raw_input('Enter directory prefix: ')

sPrefix = string.strip(sPrefix)
sPrefix = string.rstrip(sPrefix,'/')
def buildPath(str):
    global sPrefix
    str = sPrefix+'/share/divifund/'+str
    return str

sPyPath = sys.path
allmods = ['','']
def addMods(myMods,sDirName,curNames):
    for thisName in curNames:
        allmods.append(thisName)
        
print 'Finding all installed modules...'        
for sp in sPyPath:
    os.path.walk(sp,addMods,allmods)

ierr = 0
ibreak = 0
print 'Checking for gobject...'
for item in allmods:
    if item=='gobject.so':
        ibreak =1
        break
if ibreak == 0:
    ierr = ierr +1
    print 'gobject not installed. You need to install this for Divifund'
else:
    print 'libgobject OK'
ibreak = 0

print 'Checking for gtk...'
for item in allmods:
    if item=='_gtk.so':
        ibreak =1
        break
if ibreak == 0:
    ierr = ierr +1
    print 'gtk not installed. You need to install this for Divifund'
else:
    print 'gtk OK'

print 'Checking for libglade...'
for item in allmods:
    if item=='glade.so':
        ibreak =1
        break
if ibreak == 0:
    ierr = ierr +1
    print 'glade not installed. You need to install this for Divifund'
else:
    print 'libglade OK'


print 'Checking for gconf...'
for item in allmods:
    if item=='gconf.so':
        ibreak =1
        break
if ibreak == 0:
    print 'Warning: gconf not installed, this is optional but highly recommended for platforms it supports.'
else:
    print 'gconf OK'

if ierr > 0:
    print ' Can not install Divifund, please supply the needed modules '
    exit()
    
fin = open('Divifund.py','r')
iFound =0
m_sOut = ['','']
quote = "'"
count = 0
libPath = sPrefix+'/lib/divifund'
for line in fin:
    if(iFound ==0) and (string.find(line,'sPREFIX') >= 0):
        line = 'sPREFIX = '+quote+sPrefix+'/share'+quote+' \n'
        print 'Support files will be installed in ',sPrefix+'/share'
        print 'The path to the binary is ',sPrefix+'/bin/Divifund'
        iFound = 1
    if(iFound ==1) and (string.find(line,'libPath =') >= 0):
        line = 'libPath = '+quote+sPrefix+'/lib/divifund'+quote+' \n'
        print 'Library files will be installed in ',sPrefix+'/lib/divifund'
        iFound = 2
    if count < 2:
        m_sOut[count] = line
    else:
        m_sOut.append(line)
    count = count + 1
fin.close()

if not(os.path.exists(sPrefix)):
    os.makedirs(sPrefix)
    os.chmod(sPrefix,493)
sDivifundPath = buildPath('')

if not(os.path.exists(sDivifundPath)):
    os.makedirs(sDivifundPath)
    os.chmod(sDivifundPath,493)
    
if not(os.path.exists(libPath)):
    os.makedirs(libPath)
    os.chmod(libPath,493)

sBinPath = sPrefix+'/bin'
if not(os.path.exists(sBinPath)):
    os.makedirs(sBinPath)
    os.chmod(sBinPath,493)
sBinBinPath = sBinPath+'/Divifund'
fout = open(sBinBinPath,'w')
for line in m_sOut:
    fout.write(line)
fout.close()
os.system('chmod +x '+sBinBinPath)

cmdlin = 'cp generic-budget.txt '+sDivifundPath
os.system(cmdlin)
cmdlin = 'cp splashscreen.png '+sDivifundPath
os.system(cmdlin)
cmdlin = 'cp example.myb '+sDivifundPath
os.system(cmdlin)

gladePath = buildPath('glade')
if not(os.path.exists(gladePath)):
    os.makedirs(gladePath)
    os.chmod(gladePath,493)
cmdlin = 'cp glade/* '+gladePath
os.system(cmdlin)


if not(os.path.exists(libPath)):
    os.makedirs(libPath)
    os.chmod(libPath,493)
cmdlin = 'cp xptools.* '+libPath
os.system(cmdlin)
cmdlin = 'cp xpconf.* '+libPath
os.system(cmdlin)
cmdlin = 'cp exportGnumeric.* '+libPath
os.system(cmdlin)
cmdlin = 'cp exportAbiWord.* '+libPath
os.system(cmdlin)
cmdlin = 'cp importGNUCash.* '+libPath
os.system(cmdlin)
cmdlin = 'cp utilTools.* '+libPath
os.system(cmdlin)
cmdlin = 'cp DivifundClass.* '+libPath
os.system(cmdlin)
cmdlin = 'cp cs_fsconf.* '+libPath
os.system(cmdlin)
cmdlin = 'cp cs_gconf.* '+libPath
os.system(cmdlin)
cmdlin = 'cp cs_middle.* '+libPath
os.system(cmdlin)

appPath = buildPath('applications')
if not(os.path.exists(appPath)):
    os.makedirs(appPath)
    os.chmod(appPath,493)
cmdlin = 'cp Divifund.desktop '+ appPath
os.system(cmdlin)


iconPath = buildPath('pixmaps')
if not(os.path.exists(iconPath)):
    os.makedirs(iconPath)
    os.chmod(iconPath,493)
cmdlin = 'cp Divifund-icon.svg '+ iconPath
os.system(cmdlin)

docPath = buildPath('')
if not(os.path.exists(docPath)):
    os.makedirs(docPath)
    os.chmod(docPath,493)
cmdlin = 'cp -r docs '+ docPath
os.system(cmdlin)

poPath = buildPath('po')
if not(os.path.exists(poPath)):
    os.makedirs(poPath)
    os.chmod(poPath,493)
cmdlin = 'cp po/*.po '+ poPath
os.system(cmdlin)
