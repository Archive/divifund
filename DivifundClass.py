#!/usr/bin/env python
#
#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


import sys
import os
import string
import gobject
import gtk
import gtk.glade
import gettext
import webbrowser

#from gtk import True, False

#
# Some useful code that should be available in all modules
#
from utilTools import *

# Debug Messages Value
# Set to TRUE to turn on debug printing
# Set to FALSE to hide all but critical messages for end users
debugMsgs=True


class Divifund:
    """Main class for Divifund"""
    def __init__(self,sPrefix):
        global _
        self.sPREFIX=sPrefix
        import xpconf
        import xptools
        self.m_OS=xptools.XP_Detect()
        self.m_pDelim = self.m_OS.getDelim()

        if debugMsgs:
            print('RP: sPREFIX:'+self.sPREFIX)
            print('RP: self.m_pDelim:'+self.m_pDelim)
#
# Member variables and structures.
#
# self.m_iCurrentCatIndex // Points to the current selected item in caterogies
# self.m_pXML // The glade class that holds the interface
# self.m_wCatEntry  // The catergory entry 
# self.m_wDateEntry  // The Date entry widget
# self.m_wAmountEntry  // The amount entry widget
# self.m_wCommentsText // The Comments TextView
# self.m_wDateSelector // The Date selector widget
# self.m_wAddButton  // The "add" buttom
# self.m_allItems // The list Of catergories (maj,min,Budget,Num/year,First due)


#
# Fire up the splashscreen
#
        splashDelay =3000
        wSplash = gtk.Window(gtk.WINDOW_POPUP )
        wSplash.set_decorated(False)
        wSplashScreen = gtk.Image()
        sSplash = self.buildPath('splashscreen.png')
        if debugMsgs:
            print 'MSEVIOR: splashscreen path ',sSplash
            print 'MSEVIOR: exists.. ',os.path.exists(sSplash)
        wSplashScreen.set_from_file(sSplash)
        wSplashScreen.show()
# Make a pretty frame

        wSplashFrame = gtk.Frame()
        wSplashFrame.set_shadow_type(gtk.SHADOW_OUT)
        wSplashFrame.add(wSplashScreen)
        wSplashFrame.show()
        wSplash.add(wSplashFrame)
#
# The idle timeout handler to destroy the splashscreen
#
        def destroySplash(splashScreen):
            splashScreen.destroy()
            return False

#
# OK throw up the splashscreen
#
        wSplash.set_position(gtk.WIN_POS_CENTER_ALWAYS)
        wSplash.show()
        gtk.main_iteration(True)
        for i in range(5):
            gtk.main_iteration()

        gobject.timeout_add(splashDelay,destroySplash,wSplash)

#
# Load Import/Export Objects
#
        import exportGnumeric
        import exportAbiWord
        import importGNUCash
        
        self.m_ExportGnumeric = exportGnumeric.ExportGnumeric()
        self.m_ExportAbiWord = exportAbiWord.ExportAbiWord()
        self.m_impGNUCash = importGNUCash.ImportGNUCash()
        
        fname = self.buildPath('glade'+self.m_pDelim+'divifund.glade')
 
 
# create widget tree ...

        self.m_pXML = gtk.glade.XML(fname)

        gettext.textdomain('Divifund')
        gettext.bindtextdomain('Divifund', self.buildPath('po'+self.m_pDelim))
        _ = gettext.gettext

#
# Initialize xpconf properly
# appKey, the parameter, would be "/app/appKey/key" in gconf speak.
#
        self.m_xpconf = xpconf.settings('Divifund')

        self.m_sInitialFile = None
        if len(sys.argv) > 1:
            self.m_sInitialFile = sys.argv[1]
            if os.path.exists(self.m_sInitialFile):
                self.m_sInitialFile = None

        class RecentFiles:
            """Transperently handles recent files via config info"""

            def __init__(self,numRecent,wRecentMenu,Divi):
                self.Divi = Divi
                self.m_iRecent = 0
                self.m_MaxRecent = numRecent
                self.m_RecentStack =['']
                self.m_wRecentMenuBase = wRecentMenu
#
# Now fill a recent file stack from config info
#
                if self.m_MaxRecent > 0:
                    base_key = 'Recent_'
                    key = base_key + '1'
                    first = self.Divi.m_xpconf.readValue(key)
                    if first != None:
                        self.m_RecentStack = [first]
                        self.m_iRecent = 1
                        i = 2
                        while i <= numRecent:
                            key = base_key + ('%d' % i)
                            first =  self.Divi.m_xpconf.readValue(key)
                            if first == None:
                                break
                            self.m_RecentStack.append(first)
                            self.m_iRecent += 1
                            i += 1
                self.buildRecentMenu()
                return
    
            def onRecent_cb(self,me):
                if self.Divi.m_iDirty > 0:
                    res = querySave()
                    if res == gtk.RESPONSE_REJECT:
                        return
                newFile = me.get_data('myName')
                self.Divi.doOpenFile(newFile)
                return

            def buildRecentMenu(self):
                iSize = len(self.m_RecentStack)
                self.m_wRecentMenu = gtk.Menu()
                self.m_wRecentMenuBase.set_submenu(self.m_wRecentMenu)
                for i in range(iSize):
                    wMenuItem = gtk.MenuItem(self.getNthRecentShort(i+1))
                    wMenuItem.set_data('myName',self.getNthRecent(i+1))
                    self.m_wRecentMenu.append(wMenuItem)
                    wMenuItem.connect('activate',self.onRecent_cb)
                self.m_wRecentMenu.show_all()
                return
    
            def getNumRecent(self):
                return self.m_iRecent

            def getMaxRecent(self):
                return self.m_MaxRecent

            def getNthRecentShort(self,nth):
                if nth > self.m_iRecent :
                    return None
                short = self.m_RecentStack[nth-1]
                ishort = len(short)
                i = ishort-1
                while (i >= 0) and (short[i] != self.Divi.m_pDelim):
                    i = -1
                if i < 0:
                    return short
                return short[i,ishort]

            def getNthRecent(self,nth):
                if nth > self.m_iRecent :
                    return None
                return self.m_RecentStack[nth-1]

            def pushRecent(self,sNewRecent):
#
# Look for pre-existing recent file
#
#        print 'pushRecent file ',sNewRecent
                i = 0
                for sRecent in self.m_RecentStack:
                    if (sRecent == sNewRecent) and (i != 0):
                        self.m_RecentStack.remove(self.m_RecentStack[i])
                        break
                    elif (sRecent == sNewRecent) and (i == 0):
                        return
                    else:
                        i += 1
                self.m_RecentStack.insert(0,sNewRecent)
                self.m_iRecent = len(self.m_RecentStack)
                while self.m_iRecent > self.m_MaxRecent:
                    self.m_RecentStack.remove(self.m_RecentStack[self.m_iRecent-1])
                    self.m_iRecent -= 1
        
                base_key = 'Recent_'
                i = 0
                for item in self.m_RecentStack:
                    j = i+1
                    key = base_key + ('%d' % j)
                    self.Divi.m_xpconf.setValue(key,item)
 #               print 'saving ith ',i,' key ',key,' value ',item
                    i += 1
                self.m_iRecent = len(self.m_RecentStack)
                self.m_wRecentMenu.destroy()
                self.buildRecentMenu()
                return
#
# End of class definition
#
#
# Read in the Categories and make the result globally available
#
        self.m_allItems = [['','','','','',''],['','','','','','']]
        self.m_allRecords = [['','','','',''],['','','','','']]
        self.m_iNumRecords = 0
        self.m_LastBudgetRecord =['','','','','','']
#
# Set up the global variables
#
        self.m_iEditCurrent = -1
        self.m_iBudEditCurrent = -1
        self.m_iCurrentCatIndex = 0
        self.m_sCurrentFile = ''
        self.m_iDirty = 0
        self.m_iResultsCatSelected = 0

        self.m_wCatMajEntry  = self.m_pXML.get_widget('wCatMajEntry')
        self.m_wCatMajEntry.set_property('editable',False)
        self.m_wCatMinEntry  = self.m_pXML.get_widget('wCatMinEntry')
        self.m_wCatMinEntry.set_property('editable',False)
        self.m_wDateEntry = self.m_pXML.get_widget('wEntryDate')
        self.m_wAmountEntry =  self.m_pXML.get_widget('wEntryAmount')
        self.m_wCommentsText = self.m_pXML.get_widget('wTextComments')
        self.m_wDateSelector = self.m_pXML.get_widget('wCalendarSelector')
        self.m_wAddButton =  self.m_pXML.get_widget('wAddButton')
        self.m_wTBSaveButton = self.m_pXML.get_widget('wTBSaveButton')
        self.m_wMenuSave = self.m_pXML.get_widget('wMenuSave')
        self.m_wMenuSaveAs = self.m_pXML.get_widget('wMenuSaveAs')
        self.m_wMenuOpen = self.m_pXML.get_widget('wMenuOpen')
        self.m_wMenuNew = self.m_pXML.get_widget('wMenuNew')
        self.m_wMenuAbout = self.m_pXML.get_widget('wMenuAbout')
        self.m_wRecentFiles = self.m_pXML.get_widget('wRecentFiles')
        self.m_wMenuHelp = self.m_pXML.get_widget('wMenuHelp')
        self.m_wMenuExample = self.m_pXML.get_widget('wMenuExample')
        self.m_wTBOpen = self.m_pXML.get_widget('wTBOpen')
        self.m_wTBNew = self.m_pXML.get_widget('wTBNew')
        self.m_wMainWin = self.m_pXML.get_widget('wBudgetApp')
        self.m_wMainReplace = self.m_pXML.get_widget('wMainReplace')
        self.m_wMainDelete = self.m_pXML.get_widget('wMainDelete')
        self.m_wMainSort = self.m_pXML.get_widget('wMainSort')
        self.m_wResultsCatWindow = self.m_pXML.get_widget('wResultsCatWindow')
        self.m_wResultsDateLow = self.m_pXML.get_widget('wResultDateLow')
        self.m_wResultsDateHigh = self.m_pXML.get_widget('wResultDateHigh')
        self.m_wResultsWindow = self.m_pXML.get_widget('wResultsWindow')
        self.m_wResultsApply = self.m_pXML.get_widget('wResultsApply')
        self.m_wTotalSpent = self.m_pXML.get_widget('wTotalSpent')
        self.m_wResultsAllCategories = self.m_pXML.get_widget('wResultsAllCategories')
        self.m_iResultsAllCatsChosen = 0;
        self.m_wAmountBudgeted = self.m_pXML.get_widget('wAmountBudgeted')
        self.m_wDDExpended = self.m_pXML.get_widget('wDDExpended')
        self.m_wDDBudgeted = self.m_pXML.get_widget('wDDBudgeted')
        self.m_wSortDates = self.m_pXML.get_widget('wSortDates')
        self.m_wNoteBook = self.m_pXML.get_widget('wNoteBook')
        self.m_wTBExportAbi = self.m_pXML.get_widget('wExportAbi')
        self.m_wTBExportGnumeric = self.m_pXML.get_widget('wExportGnumeric')
        self.m_wMenuExportAbi = self.m_pXML.get_widget('wMenuExportAbiWord')
        self.m_wMenuExportGnumeric = self.m_pXML.get_widget('wMenuExportGnumeric')
# TAB counters

        self.defEnterOrEdit = 0
        self.defTotals = 1
        self.defBudget = 2
        self.defDirectDebit = 3
        self.defImportData = 4

# Budget Tab widgets

        self.m_wBudReplace = self.m_pXML.get_widget('wBudgetReplace')
        self.m_wBudSort = self.m_pXML.get_widget('wBudgetSort')
        self.m_wBudAdd = self.m_pXML.get_widget('wBudgetAdd')
        self.m_wBudDelete = self.m_pXML.get_widget('wBudgetDelete')
        self.m_wBudNew = self.m_pXML.get_widget('wBudgetNew')

        self.m_wBudMajEntry = self.m_pXML.get_widget('wBudgetMajCat')
        self.m_wBudSubCatEntry = self.m_pXML.get_widget('wBudgetSubCat')
        self.m_wBudYearlyEntry = self.m_pXML.get_widget('wBudgetYearlyBudget')
        self.m_wBudNumSchedSpin = self.m_pXML.get_widget('wNumSchedSpin')
        self.m_wBudDateFirstEntry = self.m_pXML.get_widget('wBudgetDateFirst')
        self.m_wBudChooseFirstDateButton = self.m_pXML.get_widget('wBudgetChooseFirstDate')
        self.m_wBudCashOrDDEntry = self.m_pXML.get_widget('wBudgetCashOrDirectDebit')

        self.m_wBudYearTotalEntry = self.m_pXML.get_widget('wBudgetYearTotal')
        self.m_wBudDDTotalEntry = self.m_pXML.get_widget('wBudgetDDTotal')
        self.m_wBudDDTargetDateEntry = self.m_pXML.get_widget('wBudgetDDTargetDate')
        self.m_wBudChooseDDTargetButton = self.m_pXML.get_widget('wBudgetDDChoose')
        self.m_wBudTargetNeededEntry = self.m_pXML.get_widget('wBudgetDDAtDate')

        self.m_wBudgetWindow = self.m_pXML.get_widget('wBudgetWindow')

# Direct Debit Tab widgets

        self.m_wDDDateChosenEntry = self.m_pXML.get_widget('wDDDateChosenEntry')
        self.m_wDDCalendar = self.m_pXML.get_widget('wDDCalendar')
        self.m_lbDDResults = self.m_pXML.get_widget('lbDDResults')
        self.m_wDDWindow = self.m_pXML.get_widget('wDDWindow')

        self.m_iResultDateLow = 0
        self.m_iResultDateHigh = 999999
        self.m_fAllExpenses = 0.0
        self.m_fDDExpenses = 0.0
        self.m_fAllBudget = 0.0
        self.m_fDDBudget = 0.0
        self.m_fBudgetTotalYear = 0.0
        self.m_fBudgetTotalDD = 0.0
        self.m_fBudgetTargetDD = 0.0

# Import Tab widgets

        self.m_wImpCatWindow = self.m_pXML.get_widget('wImportCatWindow')
        self.m_wImpCal = self.m_pXML.get_widget('wImportCal')
        self.m_wImpCat1 = self.m_pXML.get_widget('wImportCat1')
        self.m_wImpCat2 = self.m_pXML.get_widget('wImportCat2')
        self.m_wImpDateCal = self.m_pXML.get_widget('wImportCal')
        self.m_wImpDateEntry = self.m_pXML.get_widget('wImportDate')
        self.m_wImpAmount = self.m_pXML.get_widget('wImportAmount')
        self.m_wImpDesc = self.m_pXML.get_widget('wImportDescText')
        self.m_wImpImpWindow = self.m_pXML.get_widget('wImportImportWindow')
        self.m_wImportDataButton = self.m_pXML.get_widget('wImportData')
        self.m_wImportAddButton = self.m_pXML.get_widget('wImportAdd')
        self.m_wImportDeleteButton = self.m_pXML.get_widget('wImportDelete')
#
# Class to deal with Recent Files
#
        self.m_RecentFiles = RecentFiles(4,self.m_wRecentFiles,self)
        if self.m_sInitialFile == None:
            self.m_sInitialFile = self.m_RecentFiles.getNthRecent(1)

        self.m_monthDays = [31,28,31,30,31,30,31,31,30,31,30,31]
#
# More initial building of stuff
#
        if self.m_sInitialFile == None:
            self.m_wRecordTree = self.buildRecordTree('')
        elif os.path.exists(self.m_sInitialFile):
            self.m_wRecordTree = self.buildRecordTree(self.m_sInitialFile)
        else:
            self.m_wRecordTree = self.buildRecordTree('')

        self.m_EditIter = self.m_wRecordTree.get_model().get_iter_root()

        if self.m_sInitialFile == None:
            self.m_sInitialFile = ''
    
        self.m_wCategoryTree = self.createCategoryTree(self.m_sInitialFile)
        self.m_wResultsCategoryTree = self.createResultsCategoryTree(self.m_sInitialFile)

        self.m_fDDBudget = 0.0
        self.m_fDDExpens = 0.0
        self.m_fFullBudget = 0.0
        self.m_fFullExpense = 0.0
        
        self.m_iFirstRun = 1
        self.m_sResultsHeadings = [_('Category  '),('Total Expended  ')]
        self.m_sResultsHeadings.append(_('Budget for Period '))
        self.m_sResultsHeadings.append(_('Budget for Year '))
        self.m_sResultsHeadings.append(_('Direct Debit  '))
        self.m_sResultsHeadings.append(_('Difference '))

        self.m_sRecordHeadings = [_('Category  '),_('Sub Category  ')]
        self.m_sRecordHeadings.append(_('Date '))
        self.m_sRecordHeadings.append(_('Amount '))
        self.m_sRecordHeadings.append(_('Comments '))

        self.m_wResultsTree = self.buildResultsTree()

        self.m_sBudgetHeadings = [_('Category  '),_('Sub Category  ')]
        self.m_sBudgetHeadings.append(_('Yearly Budget '))
        self.m_sBudgetHeadings.append(_('Num Scheduled Payments '))
        self.m_sBudgetHeadings.append(_('Date First Payment '))
        self.m_sBudgetHeadings.append(_('Cash or Direct Debit '))


        self.m_wBudgetTree = self.buildBudgetTree()

        self.m_sBudDay = ""
        self.m_sDDTotal = ''

        self.m_sDDDate = ''
        self.m_fDDSavings = 0.0
        self.m_sDDHeadings = [_('Category  '),_('Total Expended  ')]
        self.m_sDDHeadings.append(_('Budget to Date '))
        self.m_sDDHeadings.append(_('Next due Date '))
        self.m_sDDHeadings.append(_('Budget for Year '))
        self.m_sDDHeadings.append(_('Amount needed to be Saved '))

        self.m_wDDResultsTree = self.buildDDResultsTree()
        self.m_wDDWindow.add(self.m_wDDResultsTree)
        self.m_wDDWindow.show_all()

# Initialize Import TAB

        self.m_wImportCategoryTree = self.buildImportCategoryTree()
        self.m_iHaveImportData = 0
        self.m_allImpRecords = [['','','','',''],['','','','','']]
        self.m_wImportRecTree = self.createImportRecordsTree()
        self.m_iEditImport = 0
        self.m_sImportFile = ''
#
# Import File type supported by Divifund. At present these are
# 0 unknown
# 1 gnucash use class importGNUCash to import it
# 2 quicken version 2000 , use class importQuicken200 to import it
# etc
        self.m_iImportFileType = 0
# END OF __INIT__

#
# The build path method
#
    def buildPath(self,str):
        spath = self.sPREFIX+self.m_pDelim+'divifund'+self.m_pDelim+str
        return spath
#
# Read in the category file and build our categy data structure
#
    def readCategories(self,fileModel):
        if fileModel == '' :
            inCat = open(self.buildPath('generic-budget.txt'),'r')
        else :
            inCat = open(fileModel,'r')
            self.m_RecentFiles.pushRecent(fileModel)
        inCatStrs = inCat.readlines()
        allFrags = [['','','','','',''],['','','','','','']]
        j = 0
        for s in inCatStrs:
            if s[:6] == '======' :
                break
            frags = string.split(s,':')
            i = 0
            for ss in frags:
                frags[i] = trimString(ss)
                i = i + 1
            if j < 2:
                allFrags[j] = frags
            else:
                allFrags.append(frags)

            j = j +1
        inCat.close()
        return allFrags

#
# Validate a string as a valid date
#
    def validateDate(self,str):
        s = trimString(str)
        try:
            d,m,y = string.split(s,'/')
            return 1
        except:
            return 0
#
# Validate a month/day string as a valid date
#
    def validateMonthDate(self,str):
        s = trimString(str)
        try:
            d,m = string.split(s,'/')
            return 1
        except:
            return 0

#
# Validate a string as a valid floating point number
#
    def validateNum(self,str):
        s = string.strip(str)
        if s[0] == '$':
            ss = s[1:]
        else:
            ss = s
        try:
            fl = float(ss)
            return 1
        except:
            return 0
#
# Convert a text string of the form "day/month/year" eg 03/04/02 to an
# interger number which is the number of days since Jan 1st, 2000. Assume
# month ==1 => January
#
    def daysFromDate(self,str):
        s = trimString(str)
        myDay,myMon,myYear = string.split(s,'/')
        iYear = string.atoi(myYear)

# Jan == Month 1

        iMon = string.atoi(myMon)-1 
        iDay = string.atoi(myDay)
        iYear = iYear -2000
        iYearDays = iYear*365
        iExtra = 0
        iExtra = iYear/4
        iYearDays = iYearDays+iExtra
        iLeap = iYear/4
        if iLeap*4 == iYear:
            iLeap = 1
        else:
            iLeap = 0
        monDays = 0
        i = 0
        for mon in self.m_monthDays:
            monDays = monDays + mon
            if (mon == 28) and (iLeap == 1):
                monDays = monDays + 1
            if i== iMon:
                break
            i = i + 1
        iYearDays = iYearDays + monDays + iDay
        return iYearDays


#
# Convert the number of days since Jan 1, 2000 to a date string
# month ==1 => January
#
    def dateFromDays(self,iDays):
        totDays = 0
        prevDays = 0
        iYear = 2000
        iLeapCount = 0
        while totDays < iDays:
            iYear += 1
            iLeapCount += 1
            prevDays = totDays
            totDays += 365
            iLeap = 0
            if iLeapCount == 4:
                iLeapCount = 0
                iLeap = 1
                totDays += 1
        iYear -= 1
        iDays -= prevDays
        iCount = 1
        for mon in self.m_monthDays:
            prevDays = iDays
            iDays -= mon
            if (mon == 28) and (iLeap == 1):
                iDays -= 1
            if iDays <= 0:
                break
            iCount += 1
        iDays = prevDays
        sDate = '%d/%d/%d' % (iDays,iCount,iYear)
        return sDate

#
# Build a category Model from a pre-existing Budget contained in
# the glocal self.m_allItems
#
    def buildCategoryModel(self):
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_INT,
                              gobject.TYPE_INT,
                              gobject.TYPE_INT)
#
# Start model here
#
        outer = 0
        inner = 0
        index  = 0
        item = self.m_allItems[0]
        prevOuter = item[0]
        iter = store.append(None)
        store.set(iter, 0, item[0],
                  1, outer,
                  2,inner,
                  3,index)
        outer = outer + 1
        index = index + 1
        for item in self.m_allItems:
            if(len(item) < 3) :
                break
            if item[0] != prevOuter:
                inner = 0
                iter = store.append(None)
                store.set(iter, 0, item[0],
                          1, outer,
                          2,inner,
                          3,index)
                prevOuter = prevOuter[0:0] + item[0]
                outer = outer + 1
            nitem = item[1]
            inner = inner + 1
            citer = store.append(iter)
            store.set(citer,0,nitem,
                      1,outer,
                      2,inner,
                      3,index)
            index = index + 1
        return store
    
#
# Create a Category Model from a file containing
# budget or a default budget if fileModel is None
#
    def create_model(self,fileModel):
        self.m_allItems = self.readCategories(fileModel)
        store = self.buildCategoryModel()
        return store

#
# Create a Category Tree in the Main window from a file containing
# budget or a default budget if fileModel is None
#
    def createCategoryTree(self,fileModel):
        catmodel = self.create_model(fileModel)
        catwin = self.m_pXML.get_widget('catWindow')
        catTree = gtk.TreeView(catmodel)
        catTree.set_rules_hint(True)
        column = gtk.TreeViewColumn(_('Categories'), gtk.CellRendererText(),
                                    text=0)
        catTree.append_column(column)
        catwin.add(catTree)
        catwin.show_all()
        return catTree

#
# Rebuild the Category Tree in the Main window from a pre-existing
# self.m_allItems
#
    def rebuildCategoryTree(self):
        catmodel = self.buildCategoryModel()
        catwin = self.m_pXML.get_widget('catWindow')
        catTree = gtk.TreeView(catmodel)
        catTree.set_rules_hint(True)
        column = gtk.TreeViewColumn(_('Categories'), gtk.CellRendererText(),
                                    text=0)
        catTree.append_column(column)
        catwin.add(catTree)
        catwin.show_all()
        return catTree

#
# Callback function to tell us if there is an active selection
# in the results category window
#
    def onResultsCatSelected_cb(self,me):
        self.m_iResultsCatSelected = 1


#
# Create a Category Tree in the Results window from a file containing
# budget or a default budget if fileModel is None
#
    def createResultsCategoryTree(self,fileModel):
        catmodel = self.create_model(fileModel)
        catwin = self.m_wResultsCatWindow
        catTree = gtk.TreeView(catmodel)
        catTree.set_rules_hint(True)
        column = gtk.TreeViewColumn(_('Categories'), gtk.CellRendererText(),
                                 text=0)
        catTree.append_column(column)
        catwin.add(catTree)
        catwin.show_all()
        return catTree

#
# Rebuild the Category Tree in the Results window from a pre-existing
# self.m_allItems
#
    def rebuildResultsCategoryTree(self):
        if self.m_wResultsCategoryTree != None:
            self.m_wResultsCategoryTree.destroy()
        catmodel = self.buildCategoryModel()
        catwin = self.m_wResultsCatWindow
        catTree = gtk.TreeView(catmodel)
        catTree.set_rules_hint(True)
        column = gtk.TreeViewColumn(_('Categories'), gtk.CellRendererText(),
                                    text=0)
        catTree.append_column(column)
        catwin.add(catTree)
        catwin.show_all()
        return catTree

#
# Create the record Tree from the given input file
#
    def buildRecordTree(self,newFile):
        self.m_iNumRecords = 0
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        recwin = self.m_pXML.get_widget('wRecordWindow')
        recTree = gtk.TreeView(store)
        recTree.set_rules_hint(True)
        sel = recTree.get_selection()
        sel.set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn(_('Category  '), gtk.CellRendererText(),
                                    text=0)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Sub Category  '), gtk.CellRendererText(),
                                    text=1)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Date '), gtk.CellRendererText(),
                                    text=2)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Amount '), gtk.CellRendererText(),
                                    text=3)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Comments '), gtk.CellRendererText(),
                                    text=4)
        recTree.append_column(column)
        recwin.add(recTree)
        recwin.show_all()
        TreeStore = recTree.get_model()
        if newFile != '' :
            self.m_RecentFiles.pushRecent(newFile)
            self.m_sCurrentFile = newFile
            self.m_wMainWin.set_title('Divifund - '+newFile)
            nFile = open(newFile,'r')
            inFileStrs = nFile.readlines()
            allFrags = [['','','','',''],['','','','','']]
            j = 0
            skip = 0
            for s in inFileStrs:
                if s[:6] == '======' :
                    skip = 1
                    continue
                if skip == 0:
                    continue
                frags = string.split(s,':')
                i = 0
                for ss in frags:
                    frags[i] = trimString(ss)
                    i = i + 1
                if j < 2:
                    allFrags[j] = frags
                else:
                    allFrags.append(frags)
#
# FIXME put in exception handling here. 
#print frags
                f = dollarToFloat(frags[3])
                s = floatToStr(f)
#                print frags[0],frags[1],frags[2],s,frags[4]
                TreeStore.append(None,(frags[0],frags[1],frags[2],s,frags[4]))
                j = j +1
            nFile.close()
            self.m_allRecords = allFrags
            self.m_iNumRecords = j
        return recTree

    def chooseFile(self,sFile) :
        response = gtk.RESPONSE_CANCEL
        wFileSel = gtk.FileSelection(_('Choose File'))
        wFileSel.show_fileop_buttons()
        if sFile != '':
            wFileSel.set_filename(sFile)
        response = wFileSel.run()
        if response == gtk.RESPONSE_CANCEL:
            self.m_sCurrentFile = ""
        else:
            self.m_sCurrentFile = wFileSel.get_filename()
        wFileSel.destroy()
        return self.m_sCurrentFile


    def chooseExportFile(self,sFile) :
        response = gtk.RESPONSE_CANCEL
        wFileSel = gtk.FileSelection(_('Choose File'))
        wFileSel.show_fileop_buttons()
        if sFile != '':
            wFileSel.set_filename(sFile)
        response = wFileSel.run()
        if response == gtk.RESPONSE_CANCEL:
            sExpFile = ""
        else:
            sExpFile = wFileSel.get_filename()
        wFileSel.destroy()
        return sExpFile

    def setDirty(self):
        self.m_iDirty = 1
        self.m_wTBSaveButton.set_sensitive(True)
        self.m_wMenuSave.set_sensitive(True)

    def clearDirty(self):
        self.m_iDirty = 0
        self.m_wTBSaveButton.set_sensitive(False)
        self.m_wMenuSave.set_sensitive(False)


    def saveToFile(self,sFile):
        self.clearDirty()
        fFile = open(sFile,'w')
        self.m_wMainWin.set_title('Divifund - '+sFile)
        for item in self.m_allItems:
            line=  item[0] + ' : ' + item[1] + ' : ' + item[2] + ' : ' + item[3] + ' : ' + item[4] + ' : ' + item[5]+'\n'
            fFile.write(line)
        fFile.write('=================================================== \n')
        if len(self.m_allRecords) > 0 :
            for jtem in self.m_allRecords:
                if jtem[0] == '':
                    continue
#
# FIXME replace '\n' with ' ' in jtem[4]
#
                line=  jtem[0] + ' : ' + jtem[1] + ' : ' + jtem[2] + ' : ' + addDollar(jtem[3]) + ' : ' + jtem[4] + '\n'
                fFile.write(line)
        fFile.close()

    def ActuallySave(self):
        if self.m_iDirty == 0:
            return
        if self.m_sCurrentFile == '':
            self.m_sCurrentFile = self.chooseFile('')
        if self.m_sCurrentFile == '':
            return
        self.saveToFile(self.m_sCurrentFile)
        self.m_RecentFiles.pushRecent(self.m_sCurrentFile)
        self.m_wMainWin.set_title('Divifund - '+self.m_sCurrentFile)
    
    def on_Save_cb(self,me):
        self.ActuallySave()
    
    def on_SaveAs_cb(self,me):
        self.m_sCurrentFile = self.chooseFile(self.m_RecentFiles.getNthRecent(1))
        if self.m_sCurrentFile != "":
            self.saveToFile(self.m_sCurrentFile)
            self.m_RecentFiles.pushRecent(self.m_sCurrentFile)
            self.m_wMainWin.set_title('Divifund - '+self.m_sCurrentFile)
        return
#
# Update the record tree on a change of name in the Budget
#
    def updateRecordTree(self,budgetItem):
        newMaj = budgetItem[0]
        newMin = budgetItem[1]
        oldMaj = self.m_LastBudgetRecord[0]
        oldMin = self.m_LastBudgetRecord[1]
        icount = -1
        for recItem in self.m_allRecords:
            icount = icount +1
            if recItem[0] != oldMaj:
                continue
            if recItem[1] != oldMin:
                continue
#        print "found it!"
#        print "changing ", self.m_allRecords[icount][0],self.m_allRecords[icount][1]
            self.m_allRecords[icount][0] = newMaj
            self.m_allRecords[icount][1] = newMin
#
# Now rebuild the Record Tree from this new data
#
        self.m_wRecordTree.destroy()
        self.rebuildRecordTree()
    
    def on_MainReplace_cb(self,me):
        self.setDirty()
        TreeStore = self.m_wRecordTree.get_model()
        catMaj = self.m_wCatMajEntry.get_text()
        catMin = self.m_wCatMinEntry.get_text()
        dateText = self.m_wDateEntry.get_text()
        amountText = addDollar(self.m_wAmountEntry.get_text())
        commentBuffer = self.m_wCommentsText.get_buffer()
        commentStart = commentBuffer.get_start_iter()
        commentEnd = commentBuffer.get_end_iter()
        commentText = commentBuffer.get_text(commentStart,commentEnd,False)
        if self.m_iEditCurrent >= 0:
            self.m_allRecords[self.m_iEditCurrent] =  [catMaj,catMin,dateText,amountText,commentText]
            treeIter = TreeStore.insert_before(None,self.m_EditIter,(catMaj,catMin,
                                          dateText,amountText,commentText))
            treePath = TreeStore.get_path(treeIter)
            TreeStore.remove(self.m_EditIter)
            self.m_wRecordTree.scroll_to_cell(treePath)       

    def errorDate(self,dateText):
        response = gtk.RESPONSE_OK
        qErrDate = gtk.Dialog(_('Error in Date'),
                            self.m_wMainWin,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            (gtk.STOCK_OK, gtk.RESPONSE_OK))
        vbox = qErrDate.vbox
        sStr = _('You have entered an invalid date \n')
        sStr += dateText
        sStr += _('\nPlease re-enter a correctly formatted date. \n')
        label = gtk.Label(sStr)
        vbox.add(label)
        qErrDate.show_all()
        response = qErrDate.run()
        qErrDate.destroy()
        return


    def errorNumber(self,numText):
        response = gtk.RESPONSE_OK
        qErrNum = gtk.Dialog(_('Error in Number'),
                            self.m_wMainWin,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            (gtk.STOCK_OK, gtk.RESPONSE_OK))
        vbox = qErrNum.vbox
        sStr = _('You have entered an invalid number \n')
        sStr += numText
        sStr += _('\n Please reenter a valid number. \n')
        label = gtk.Label(sStr)
        vbox.add(label)
        qErrNum.show_all()
        response = qErrNum.run()
        qErrNum.destroy()
        return
    
    def on_AddButton_cb(self,me):
        self.setDirty()
        TreeStore = self.m_wRecordTree.get_model()
        catMaj = self.m_allItems[self.m_iCurrentCatIndex][0]
        catMin = self.m_allItems[self.m_iCurrentCatIndex][1]
        dateText = self.m_wDateEntry.get_text()
        if self.validateDate(dateText) == 0:
            self.errorDate(dateText)
            return
        amountText = addDollar(self.m_wAmountEntry.get_text())
        if self.validateNum(amountText) == 0:
            self.errorNumber(self.m_wAmountEntry.get_text())
            return
        commentBuffer = self.m_wCommentsText.get_buffer()
        commentStart = commentBuffer.get_start_iter()
        commentEnd = commentBuffer.get_end_iter()
        commentText = commentBuffer.get_text(commentStart,commentEnd,False)
        
        treeIter = TreeStore.append(None,(catMaj,catMin,
                                          dateText,amountText,commentText))
        treePath = TreeStore.get_path(treeIter)
        if self.m_iNumRecords < 2:
            self.m_allRecords[self.m_iNumRecords] = [catMaj,catMin,dateText,amountText,commentText]
        else:
            self.m_allRecords.append([ catMaj,catMin,dateText,amountText,commentText])
        self.m_iNumRecords = self.m_iNumRecords + 1
        self.m_wRecordTree.scroll_to_cell(treePath)


    def on_mainQuit_cb(self,me):
        (iWidth,iHeight) = self.m_wMainWin.get_size()
        sWidth = "%d" % iWidth
        sHeight = "%d" % iHeight
        self.m_xpconf.setValue('width',sWidth)
        self.m_xpconf.setValue('height',sHeight)
        gtk.main_quit()

    def daySelected_cb(self,me):
        year, month, day = self.m_wDateSelector.get_date()
        str = "%d/%d/%d" % (day,month+1,year)
        self.m_wDateEntry.set_text(str)

    def getResultLow(self):
        year, month, day = self.m_wResultsDateLow.get_date()
        str = "%d/%d/%d" % (day,month+1,year)
        self.m_iResultDateLow = self.daysFromDate(str)
    
    def on_ResultLow_cb(self,me):
        self.getResultLow()

    def getResultHigh(self):
        year, month, day = self.m_wResultsDateHigh.get_date()
        str = "%d/%d/%d" % (day,month+1,year)
        self.m_iResultDateHigh = self.daysFromDate(str)

    def on_ResultHigh_cb(self,me):
        self.getResultHigh()

    def catTreeSelection_cb(self,selection):
        selection = selection.get_selected()
        if not selection:
            return
        model, iter = selection
        item_str = model.get_value(iter,0)
        item_outer = model.get_value(iter,1)
        item_inner = model.get_value(iter,2)
        item_index = model.get_value(iter,3) -1
        self.m_iCurrentCatIndex = item_index
#    print 'selected item ',item_str,' outer index ',item_outer,' inner index ',item_inner,' Total index ',item_index
#    str = m_allItems[item_index][0] + ' - ' + m_allItems[item_index][1]
#    print ' selected cat is ',str
#    print 'Current Cat index is ',m_iCurrentCatIndex
        self.m_wCatMajEntry.set_text(self.m_allItems[item_index][0] )
        self.m_wCatMinEntry.set_text(self.m_allItems[item_index][1] )


    def querySave(self):
        response = gtk.RESPONSE_NO
        if self.m_iDirty == 0 :
            return response
        qSave = gtk.Dialog("Query Save",
                           self.m_wMainWin,
                           gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                           (gtk.STOCK_SAVE, gtk.RESPONSE_APPLY,
                            gtk.STOCK_NO, gtk.RESPONSE_NO,
                            gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT))
        vbox = qSave.vbox
        label = gtk.Label(_('The current document has not been saved.\n If you do not save now some changes will be lost. \n Do you wish to save your document now?'))
        vbox.add(label)
        qSave.show_all()
        response = qSave.run()
        qSave.destroy()
        if response == gtk.RESPONSE_APPLY :
            self.ActuallySave()
        return response

    def saveAndQuit_cb(self,me):
        response =  self.querySave()
        if response == gtk.RESPONSE_REJECT :
            return
        self.on_mainQuit_cb(me)

    def SaveAndQuit(self):
        response = self.querySave()
        if response == gtk.RESPONSE_REJECT :
            return
        gtk.main_quit()

    def cleanBudget(self):
        self.m_wRecordTree.destroy()
        self.m_wCategoryTree.destroy()
        self.m_wBudgetTree.destroy()
        self.m_wResultsCategoryTree.destroy()
    
    def on_New_cb(self,me):
        if self.m_iDirty > 0:
            res = querySave()
            if res == gtk.RESPONSE_REJECT:
                return
        self.cleanBudget()
        self.m_wCategoryTree = self.createCategoryTree('')
        self.m_wResultsCategoryTree = self.createResultsCategoryTree('')
        sel = self.m_wCategoryTree.get_selection()
        sel.set_mode (gtk.SELECTION_BROWSE)
        sel.connect('changed',self.catTreeSelection_cb)
        sel2 = self.m_wResultsCategoryTree.get_selection()
        sel2.set_mode(gtk.SELECTION_MULTIPLE)
        sel2.connect('changed',self.onResultsCatSelected_cb)
        self.m_iResultsCatSelected = 0
        self.m_wRecordTree = self.buildRecordTree('')
        self.m_wBudgetTree = self.buildBudgetTree()
        sel3 = self.m_wBudgetTree.get_selection()
        sel3.set_mode (gtk.SELECTION_BROWSE)
        sel3.connect('changed',self.budgetTreeSelection_cb)
        sel4 = self.m_wRecordTree.get_selection()
        sel4.set_mode (gtk.SELECTION_BROWSE)
        sel4.connect('changed',self.recordTreeSelection_cb)

#
# Read in a new Budget and record file
#

    def on_Open_cb(self,me):
 #    print ' Open button pressed '
        sPrevPath=''
        if self.m_iDirty > 0:
            res = self.querySave()
            if res == gtk.RESPONSE_REJECT:
                return
        
        sPrevPath = self.m_RecentFiles.getNthRecent(1)
        # print sPrevPath
        if sPrevPath != None:
            sPrevPath = self.getPathOnly(sPrevPath)
        if sPrevPath != None:
            newFile = self.chooseFile(sPrevPath)
        else:
            newFile = self.chooseFile('')
        if newFile == "":
            return
        self.doOpenFile(newFile)
        return
#
# Actually read it in now
#
    def doOpenFile(self,newFile):
        self.m_wMainWin.set_title('Divifund - '+newFile)
        self.cleanBudget()
        self.m_wCategoryTree = self.createCategoryTree(newFile)
        self.m_wResultsCategoryTree = self.createResultsCategoryTree(newFile)
        sel = self.m_wCategoryTree.get_selection()
        sel.set_mode (gtk.SELECTION_SINGLE)
        sel.connect('changed',self.catTreeSelection_cb)
        sel2 = self.m_wResultsCategoryTree.get_selection()
        sel2.set_mode(gtk.SELECTION_MULTIPLE)
        sel2.connect('changed',self.onResultsCatSelected_cb)
        self.m_iResultsCatSelected = 0
        self.m_iResultsAllCatsChosen = 0
        self.m_wRecordTree = self.buildRecordTree(newFile)
        self.m_wBudgetTree = self.buildBudgetTree()
        sel3 = self.m_wBudgetTree.get_selection()
        sel3.set_mode (gtk.SELECTION_BROWSE)
        sel3.connect('changed',self.budgetTreeSelection_cb)
        sel4 = self.m_wRecordTree.get_selection()
        sel4.set_mode (gtk.SELECTION_BROWSE)
        sel4.connect('changed',self.recordTreeSelection_cb)
     
#
# Delete the currently selected record
#
    def on_MainDelete_cb(self,me):
        treePath,Col = self.m_wRecordTree.get_cursor()
        if treePath == None:
            return
        treeModel = self.m_wRecordTree.get_model()
        iDeleteRow = treePath[0]
        deleteIter = treeModel.get_iter(treePath)
        treeModel.remove(deleteIter)
        del self.m_allRecords[iDeleteRow]

    def onResultsAllCats_cb(self,me):
        if self.m_iResultsAllCatsChosen == 0:
            if self.m_wResultsAllCategories.get_active() == True:
                sel = self.m_wResultsCategoryTree.get_selection()
                sel.select_all()
                self.m_iResultsAllCatsChosen = 1
                self.m_iResultsCatSelected = 0
        else:
            if self.m_wResultsAllCategories.get_active() == False:
                self.m_iResultsAllCatsChosen = 0
                if self.m_iResultsCatSelected == 0:
                    sel = self.m_wResultsCategoryTree.get_selection()
                    sel.unselect_all()
                    self.m_wResultsAllCategories.unselect_all()
            
#
# Returns 0 if the apply string does not match a key in the selected
# Categories.
# Returns 1 otherwise
#
    def inSelectRange(self,str):
        if self.m_wResultsAllCategories.get_active() == True: 
            return 1
        return 1

#
# This dictionary gives the index to the self.m_allItems category structure
# If the category is selected. Otherwise it returns -1
#
    def buildApplyDict(self,allItems):
        r1 = allItems[0]
        r2 = allItems[1]
        key1 = r1[0] + ' - ' + r1[1]
        key2 = r2[0] + ' - ' + r2[1]
        myDict = {key1:0, key2: 1}
        if self.inSelectRange(key1) == 0 :
            myDict[key1] = -1
        if self.inSelectRange(key2) == 0 :
            myDict[key2] = -1
        i = -1
        for row in allItems:
            i = i + 1
            if i < 2 :
                continue
            str = row[0] + ' - ' + row[1]
#        print ' key ',str,' index ',i
            if self.inSelectRange(str) == 1:
                myDict[str] = i
            else:
                myDict[str] = -1
        return myDict


# Build the results data structure.
# It's a list containing the following fields
# Combined categories, Accumulated Expenses, Budgeted Expenses, Difference
# To work efficiently we need a Dictionary data structure that maps
# the combined category strings to an index into the categories List.

    def buildResultsRange(self):
        self.getResultHigh()
        self.getResultLow()
        ApplyDic = self.buildApplyDict(self.m_allItems)
        myResults = [['bogus-1','',0.0,0.0,'',0.0],['bogus-2','',0.0,0.0,'',0.0]]
        iResults = 2
        myIndexToResults = {'bogus-1':0,'bogus-2':1}
#
# Make sure all categories are examined if we ask for all categoies
#
        if self.m_wResultsAllCategories.get_active() == True:
            dyear, dmonth, dday = self.m_wResultsDateLow.get_date()
            syear = "%d" % dyear
            for item in self.m_allItems:
                str = item[0] + ' - ' + item[1]
                if ApplyDic.has_key(str):
                    myIndexToResults[str] = iResults
                    idxToItems = ApplyDic[str]
                    sFullYear = self.m_allItems[idxToItems][2]
                    sDD = trimString(self.m_allItems[idxToItems][5])
                    strDD = _('Yes')
                    if sDD == 'C' :
                        strDD = _('No')
                    fBudget = dollarToFloat(sFullYear)
#                print ' Budget of ',str,' is ',fBudget
                    iPayments = string.atoi(self.m_allItems[idxToItems][3])
                    if iPayments == 1 :
                        sDate = self.m_allItems[idxToItems][4]
                        sDate = sDate+'/'
                        sDate = sDate+syear
                        bracketDate = self.daysFromDate(sDate)
                        if (bracketDate > self.m_iResultDateHigh) or (bracketDate  <self.m_iResultDateLow):
                            fBudget = fBudget* (self.m_iResultDateHigh - self.m_iResultDateLow)/365.0
                    else:
                        fBudget = fBudget* (self.m_iResultDateHigh - self.m_iResultDateLow)/365.0
#                print ' After spread budget is ',fBudget
                    myResults.append([str,strDD,0.0,fBudget,sFullYear,0.0])
                    iResults = iResults + 1
                else :
                    print '!!!!!! no index for key ',str
                    continue
            iResults = 2
#
# OK Entire budget is recorded if needed
#
        for curRec in self.m_allRecords:
            str = curRec[0] + ' - ' + curRec[1]
            if self.inSelectRange(str) == 0:
                continue
            day = self.daysFromDate(curRec[2])
            if (self.m_iResultDateLow > day) or (day > self.m_iResultDateHigh) :
                continue
            if myIndexToResults.has_key(str):
                i = myIndexToResults[str]
            elif ApplyDic.has_key(str):
                myIndexToResults[str] = iResults
                idxToItems = ApplyDic[str]
                sFullYear = self.m_allItems[idxToItems][2]
                sDD = trimString(self.m_allItems[idxToItems][5])
                strDD = _('Yes')
                if sDD == 'C' :
                    strDD = _('No')
                fBudget = dollarToFloat(sFullYear)
                iPayments = string.atoi(self.m_allItems[idxToItems][3])
                if iPayments == 1 :
                    sDate = self.m_allItems[idxToItems][4]
                    day,mon,year = string.split(curRec[2],'/')
                    sDate = sDate+'/'
                    sDate = sDate+year
                    bracketDate = daysFromDate(sDate)
                    if (bracketDate > self.m_iResultDateHigh) or (bracketDate  <self.m_iResultDateLow):
                        fBudget = fBudget* (self.m_iResultDateHigh - self.m_iResultDateLow)/365.0
                else:
                    fBudget = fBudget* (self.m_iResultDateHigh - self.m_iResultDateLow)/365.0
                myResults.append([str,strDD,0.0,fBudget,sFullYear,0.0])
                i = iResults
                iResults = iResults + 1
            else :
                continue
            myResults[i][2] = myResults[i][2] + dollarToFloat(curRec[3])
#
# Done building the data structure!
#
# Now calculate the difference between budget and expended.
#
        i = 0
        self.m_fDDBudget = 0.0
    
        for myRes in myResults:
            myResults[i][5] = myRes[3] - myRes[2]
            i = i +1
        return myResults

#
# decides whether to render the cell red or blue depending on whether category
# is under or over budget
#
    def renderRedOrBlue(self,column, cell, model, iter, user_data):
        value = model.get_value(iter,5)
        fvalue = dollarToFloat(value)
        if fvalue < 0 :
            cell.set_property("background","LightSalmon1")
        else:
            cell.set_property("background","LightBlue1")
#
# Fill a TreeView with the results of a scan through the data of a selected
# Range of categories.
#
    def buildResultsTree(self):
        self.m_fAllExpenses = 0.0
        self.m_fDDExpenses = 0.0
        self.m_fAllBudget = 0.0
        if self.m_iFirstRun == 1:
            store = gtk.TreeStore(gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING)
            resTree = gtk.TreeView(store)
            resTree.set_rules_hint(True)
            sel = resTree.get_selection()
            sel.set_mode (gtk.SELECTION_MULTIPLE)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[0], gtk.CellRendererText(),
                                        text=0)
            resTree.append_column(column)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[1], gtk.CellRendererText(),
                                        text=1)
            resTree.append_column(column)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[2], gtk.CellRendererText(),
                                        text=2)
            resTree.append_column(column)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[3], gtk.CellRendererText(),
                                        text=3)
            resTree.append_column(column)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[4], gtk.CellRendererText(),
                                        text=4)
            resTree.append_column(column)
            column = gtk.TreeViewColumn(self.m_sResultsHeadings[5], gtk.CellRendererText(),
                                        text=5)
            resTree.append_column(column)
            self.m_wResultsWindow.add(resTree)
            self.m_wResultsWindow.show_all()
            self.m_iFirstRun = 0
            return resTree
        myResults = self.buildResultsRange()
#
# Now build a treeView to display these as:
# Key, expended, budget, year's budget difference
#
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        resTree = gtk.TreeView(store)
        resTree.set_rules_hint(True)
        sel = resTree.get_selection()
        sel.set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn(_('Category  '), gtk.CellRendererText(),
                                    text=0)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(_('Total Expended  '), gtk.CellRendererText(),
                                    text=1)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(_('Budget for Period '), gtk.CellRendererText(),
                                    text=2)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(_('Budget for Year '), gtk.CellRendererText(),
                                    text=3)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(_('Direct Debit  '), gtk.CellRendererText(),
                                    text=4)
        resTree.append_column(column)
        cell = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('Difference '), cell,
                                    text=5)
        column.set_cell_data_func(cell,self.renderRedOrBlue,None)
        resTree.append_column(column)
        self.m_wResultsWindow.add(resTree)
        self.m_wResultsWindow.show_all()
        TreeStore = resTree.get_model()
        self.m_fDDExpenses = 0.0
        self.m_fDDBudget = 0.0
        self.m_fAllExpenses = 0.0
        self.m_fAllBudget = 0.0
        for item in myResults:
            str0 = item[0]
            if (str0 =='bogus-1') or (str0=='bogus-2'):
                continue
            str1 = floatToStr(item[2])
            str2 = floatToStr(item[3])
            ft = dollarToFloat(item[4])
            str3 = floatToStr(ft)
            str4 = item[1]
            str5 = floatToStr(item[5])
            TreeStore.append(None,(str0,str1,str2,str3,str4,str5))
            self.m_fAllExpenses = self.m_fAllExpenses + item[2]
            self.m_fAllBudget = self.m_fAllBudget + item[3]
            if str4 == "Yes" :
                self.m_fDDExpenses = self.m_fDDExpenses + item[2]
                self.m_fDDBudget = self.m_fDDBudget + item[3]
        self.m_wAmountBudgeted.set_text(floatToStr(self.m_fAllBudget))
        self.m_wTotalSpent.set_text(floatToStr(self.m_fAllExpenses))
        self.m_wDDExpended.set_text(floatToStr(self.m_fDDExpenses))
        self.m_wDDBudgeted.set_text(floatToStr(self.m_fDDBudget))
        return resTree


    def on_ApplyResultsRange_cb(self,me):
        self.m_wResultsTree.destroy()
        self.m_wResultsTree = self.buildResultsTree()
        return

    def recordTreeSelection_cb(self,me):
        treePath,Col = self.m_wRecordTree.get_cursor()
        if treePath == None:
            return
        treeModel = self.m_wRecordTree.get_model()
        self.m_iEditCurrent = treePath[0]
        self.m_EditIter = treeModel.get_iter(treePath)
        valCatMajor = treeModel.get_value(self.m_EditIter,0)
        valCatMinor = treeModel.get_value(self.m_EditIter,1)
        valDateText = treeModel.get_value(self.m_EditIter,2)
        valAmountText = treeModel.get_value(self.m_EditIter,3)
        valComment = treeModel.get_value(self.m_EditIter,4)
        self.m_wCatMajEntry.set_text(valCatMajor)
        self.m_wCatMinEntry.set_text(valCatMinor)
        self.m_wDateEntry.set_text(valDateText)
        self.m_wAmountEntry.set_text(valAmountText)
        myBuffer = gtk.TextBuffer()
        myBuffer.set_text(valComment)
        self.m_wCommentsText.set_buffer(myBuffer)
        self.m_wCommentsText.show_all()
#
# Rebuild the Record Tree from the self.m_allRecords data structure
#
    def rebuildRecordTree(self):
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        recwin = self.m_pXML.get_widget('wRecordWindow')
        recTree = gtk.TreeView(store)
        recTree.set_rules_hint(True)
        sel = recTree.get_selection()
        sel.set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn(self.m_sRecordHeadings[0], gtk.CellRendererText(),
                                    text=0)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sRecordHeadings[1], gtk.CellRendererText(),
                                    text=1)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sRecordHeadings[2], gtk.CellRendererText(),
                                    text=2)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sRecordHeadings[3], gtk.CellRendererText(),
                                    text=3)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sRecordHeadings[4], gtk.CellRendererText(),
                                    text=4)
        recTree.append_column(column)
        recwin.add(recTree)
        recwin.show_all()
        TreeStore = recTree.get_model()
        for frags in self.m_allRecords:
            f = dollarToFloat(frags[3])
            s = floatToStr(f)
            TreeStore.append(None,(frags[0],frags[1],frags[2],s,frags[4]))
        self.m_wRecordTree = recTree    
        sel = self.m_wRecordTree.get_selection()
        sel.set_mode (gtk.SELECTION_BROWSE)
        sel.connect('changed',self.recordTreeSelection_cb)

    def sortOnCategories(self,a,b):
        i = cmp(a[0],b[0])
        if i != 0 :
            return i
        i = cmp(a[1],b[1])
        if i != 0:
            return i
        adate = self.daysFromDate(a[2])
        bdate = self.daysFromDate(b[2])
        i = adate - bdate
        return i

    def sortOnDates(self,a,b):
        adate = self.daysFromDate(a[2])
        bdate = self.daysFromDate(b[2])
        i = adate - bdate
        if i != 0:
            return i
        i = cmp(a[0],b[0])
        if i != 0:
            return i
        i = cmp(a[1],b[1])
        if i != 0:
            return i
        return i

    def on_MainSortDates_cb(self,me):
        self.m_wRecordTree.destroy()
        self.m_allRecords.sort(self.sortOnDates)
        self.setDirty()
        self.rebuildRecordTree()

    def on_MainSort_cb(self,me):
        self.m_wRecordTree.destroy()
        self.m_allRecords.sort(self.sortOnCategories)
        self.setDirty()
        self.rebuildRecordTree()

    def buildBudgetTree(self):
        self.m_fBudgetTotalYear = 0.0
        self.m_fBudgetTotalDD = 0.0
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        budgetTree = gtk.TreeView(store)
        budgetTree.set_rules_hint(True)
        sel = budgetTree.get_selection()
        sel.set_mode (gtk.SELECTION_BROWSE)
        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[0], gtk.CellRendererText(),
                                    text=0)
        budgetTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[1], gtk.CellRendererText(),
                                    text=1)
        budgetTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[2], gtk.CellRendererText(),
                                    text=2)
        budgetTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[3], gtk.CellRendererText(),
                                    text=3)
        budgetTree.append_column(column)
        
        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[4], gtk.CellRendererText(),
                                    text=4)
        budgetTree.append_column(column)

        column = gtk.TreeViewColumn(self.m_sBudgetHeadings[5], gtk.CellRendererText(),
                                 text=5)
        budgetTree.append_column(column)
#
# Insert model here
#
        for item in self.m_allItems:
            iter = store.append(None)
            ft = dollarToFloat(item[2])
            s= floatToStr(ft)
            store.set(iter,0,item[0],
                      1,item[1],
                      2,s,
                      3,item[3],
                      4,item[4],
                      5,item[5])
            self.m_fBudgetTotalYear = self.m_fBudgetTotalYear + dollarToFloat(item[2])
            if(item[5] != 'C') and (item[5] != 'c') :
                self.m_fBudgetTotalDD = self.m_fBudgetTotalDD + dollarToFloat(item[2])
        str = floatToStr(self.m_fBudgetTotalYear)
        str = addDollar(str)
        self.m_wBudYearTotalEntry.set_text(str)
        self.m_fBudgetTotalDD = self.m_fBudgetTotalDD/26.0
        str = floatToStr(self.m_fBudgetTotalDD)
        str = addDollar(str)
        self.m_wBudDDTotalEntry.set_text(str)
        self.m_wBudgetWindow.add(budgetTree)
        self.m_wBudgetWindow.show_all()
        return budgetTree

    def getBudDay(self,me):
        year, month, day = me.get_date()
        self.m_sBudDay = "%d/%d/%d" % (day,month+1,year)
        return
  
    def BudDaySelected_cb(self,me):
        year, month, day = me.get_date()
        self.m_sBudDay = "%d/%d/%d" % (day,month+1,year)
        return

    def getPrevPayments(self,catMaj,catMin,sTargetDate):
        fTotal = 0.0
        iTargetDays = self.daysFromDate(sTargetDate)
        for rec in self.m_allRecords:
            if (rec[0] == catMaj) and (rec[1] == catMin):
                iDays = self.daysFromDate(rec[2])
                if iTargetDays > iDays:
                    fTotal = fTotal + dollarToFloat(rec[3])
        return fTotal

    def calculateDDTo(self,sTargetDate):
        iTargetDays = self.daysFromDate(sTargetDate)
        fTotalDD = 0.0
        targDay,targMon,targYear = string.split(sTargetDate,'/')
        for item in self.m_allItems:
            if (item[5] == 'C') or (item[5] == 'c'):
                continue
 #
 # Item is direct debit
 #
            fTotal = dollarToFloat(item[2])
            inum = string.atoi(item[3])
            sDateFirst = item[4]
            iSlash = string.count(sDateFirst,'/')
            if iSlash < 2 :
                sDateFirst = sDateFirst + '/'
                sDateFirst = sDateFirst + targYear
            if inum == 0:
#            
# Handle case of no fixed dates for payments
#
                 myDay,myMon,myYear = string.split(sTargetDate,'/')
                 iFirstDay = self.daysFromDate('1/1/'+myYear)
                 fPrevPayments = self.getPrevPayments(item[0],item[1],sTargetDate)
#             print "for ",item[0],item[1]," already paid ",fPrevPayments
#             print "first day ",iFirstDay," Target day ",iTargetDays
                 if fTotal > fPrevPayments:
                     fTotal = fTotal * (iTargetDays - iFirstDay)/365.0
                     fTotal = fTotal - fPrevPayments
                     fTotalDD = fTotalDD + fTotal
            elif inum == 1:
#
# Handle case of single yearly payment
#
#             print 'minor cat ',item[1],'Date of Payment ',sDateFirst
                 iItemDay = self.daysFromDate(sDateFirst)
                 iNumDays = 0
                 if iItemDay > iTargetDays :
                     iNumDays = 365 - (iItemDay - iTargetDays )
                 else:
                     iNumDays = iTargetDays - iItemDay
                 fTotal = fTotal*iNumDays/365.0
                 fTotalDD = fTotalDD + fTotal
            else:
#
# Handle case regular fixed payments
#
                 numDaysBetween = 0
                 numDaysBetween = 365.0/inum
                 FirstDay = self.daysFromDate(sDateFirst)
                 iNumDays = 0
                 if FirstDay > iTargetDays:
                     diff = 365 - (FirstDay - iTargetDays )
                     iNumDays = diff % numDaysBetween
                 else:
                     diff = iTargetDays - FirstDay
                     iNumDays = diff % numDaysBetween
                 fTotal = fTotal*iNumDays/(inum*numDaysBetween)
                 fTotalDD = fTotalDD + fTotal
        sTotal = floatToStr(fTotalDD)
        sTotal = addDollar(sTotal)
        return sTotal

#FIXME Keep going from here!!!
# Generate the data structure needed for the Direct Debit Pane
#
    def calculateDDResults(self,sTargetDate):
    
# Catergory,Total Expended,Budget to Date, Next due date, Budget For Year,Amount needed to be Save
        myResults = [['bogus-1',0.0,0.0,' bogus-date',0.0,0.0],['bogus-2',0.0,0.0,' bogus-date ',0.0,0.0]]
        iTargetDays = self.daysFromDate(sTargetDate)
        fTotalDD = 0.0
        targDay,targMon,targYear = string.split(sTargetDate,'/')
        for item in self.m_allItems:
            if (item[5] == 'C') or (item[5] == 'c'):
                continue
 #
 # Item is direct debit
 #
            fYearTotal = dollarToFloat(item[2])
            fTotal = fYearTotal
            inum = string.atoi(item[3])
            sDateFirst = item[4]
            iSlash = string.count(sDateFirst,'/')
            if iSlash < 2 :
                sDateFirst = sDateFirst + '/'
                sDateFirst = sDateFirst + targYear
            fPrevPayments = self.getPrevPayments(item[0],item[1],sTargetDate)
            iDaysStartYear = self.daysFromDate('1/1/'+targYear)
            iDaysFirst = self.daysFromDate(sDateFirst)
            iDaysTarget = self.daysFromDate(sTargetDate)
            fBudgetToDate = fYearTotal*(iDaysTarget-iDaysStartYear)/365.25
            sCatName = item[0] + ' - ' + item[1]
            if inum == 0:
#            
# Handle case of no fixed dates for payments
#
                 myDay,myMon,myYear = string.split(sTargetDate,'/')
                 iFirstDay = self.daysFromDate('1/1/'+myYear)
                 sDateNext = sTargetDate
#             print "for ",item[0],item[1]," already paid ",fPrevPayments
#             print "first day ",iFirstDay," Target day ",iTargetDays
                 if fTotal > fPrevPayments:
                     fTotal = fTotal * (iTargetDays - iFirstDay)/365.0
                     fTotal = fTotal - fPrevPayments
                     fTotalDD = fTotalDD + fTotal
                 else:
                     fTotal = 0.0
            elif inum == 1:
#
# Handle case of single yearly payment
#
#             print 'minor cat ',item[1],'Date of Payment ',',sDateFirst
                 iItemDay = self.daysFromDate(sDateFirst)
                 if iItemDay > iDaysTarget:
                     sDateNext = sDateFirst
                 else:
                     firstDay, firstMon, firstYear = string.split(sDateFirst,'/')
                     iYear = string.atoi(firstYear)
                     if iYear <100:
                         iYear += 2000
                     iYear += 1
                     sYear = '%d' % iYear
                     sDateNext = firstDay+'/'+firstMon+'/'+sYear
                 iNumDays = 0
                 if iItemDay > iTargetDays :
                     iNumDays = 365 - (iItemDay - iTargetDays )
                 else:
                     iNumDays = iTargetDays - iItemDay
                 fTotal = fTotal*iNumDays/365.0
                 fTotalDD = fTotalDD + fTotal
            else:
#
# Handle case regular fixed payments
#
                numDaysBetween = 0
                numDaysBetween = 365.0/inum
                FirstDay = self.daysFromDate(sDateFirst)
                iNumDays = 0
                if FirstDay > iTargetDays:
                    diff = 365 - (FirstDay - iTargetDays )
                    iNumDays = diff % numDaysBetween
                    sDateNext = self.dateFromDays(FirstDay)
                else:
                    diff = iTargetDays - FirstDay
                    iNumDays = diff % numDaysBetween
                    iNext = int(diff/numDaysBetween) + 1
                    sDateNext = self.dateFromDays(FirstDay + iNext*numDaysBetween)
                fTotal = fTotal*iNumDays/(inum*numDaysBetween)
                fTotalDD = fTotalDD + fTotal
            myResults.append([sCatName,fPrevPayments,fBudgetToDate,sDateNext,fYearTotal,fTotal])
        self.m_sDDTotal = floatToStr(fTotalDD)
        self.m_sDDTotal = addDollar(self.m_sDDTotal)
        return myResults


#
# Now build a tree showing all the results for the Direct Debit Account
#
#
# Fill a TreeView with the results of a scan through the data of all the
# the direct debit items up to the requested Date
#
    def buildDDResultsTree(self):
        year, month, day = self.m_wDDCalendar.get_date()
        self.m_sDDDate = "%d/%d/%d" % (day,month+1,year)
        self.m_wDDDateChosenEntry.set_text(self.m_sDDDate)
        myResults = self.calculateDDResults(self.m_sDDDate)
#
# Now build a treeView to display these as:
# Key, expended, budget, year's budget difference
#
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        resTree = gtk.TreeView(store)
        resTree.set_rules_hint(True)
        sel = resTree.get_selection()
        sel.set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[0], gtk.CellRendererText(),
                                    text=0)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[1], gtk.CellRendererText(),
                                    text=1)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[2], gtk.CellRendererText(),
                                    text=2)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[3], gtk.CellRendererText(),
                                    text=3)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[4], gtk.CellRendererText(),
                                    text=4)
        resTree.append_column(column)
        column = gtk.TreeViewColumn(self.m_sDDHeadings[5], gtk.CellRendererText(),
                                    text=5)
        resTree.append_column(column)
        self.m_wResultsWindow.add(resTree)
        self.m_wResultsWindow.show_all()
        TreeStore = resTree.get_model()
#    print"About to fill results data structure"
        self.m_fDDSaved = 0.0

        for item in myResults:
            str0 = item[0]
            if (str0 =='bogus-1') or (str0=='bogus-2'):
                continue
            str1 = floatToStr(item[1])
            str2 = floatToStr(item[2])
            str3 = item[3]
            str4 = floatToStr(item[4])
            str5 = floatToStr(item[5])
#        print "Got ",str1,str2,str3,str4,str5
            TreeStore.append(None,(str0,str1,str2,str3,str4,str5))
            self.m_fDDSavings += item[4]
            sLead = _("""From your budget and the amount you've already spent, you will need to have saved""")
            sFollow = _('by the date')
            sSavingsText = """<span size="xx-large">"""+sLead+""" <b> """+self.m_sDDTotal+"""</b> """+sFollow+""" <b>"""+self.m_sDDDate+"""</b></span>"""
        self.m_lbDDResults.set_label(sSavingsText)
        return resTree

    def rebuildDDTree(self):
        self.m_wDDResultsTree.destroy()
        self.m_wDDResultsTree = self.buildDDResultsTree()
        self.m_wDDWindow.add(self.m_wDDResultsTree)
        self.m_wDDWindow.show_all()
        return

    def onDDDateChanged_cb(self,me):
        self.rebuildDDTree()
        return

    def on_ChooseDDTarget_cb(self,me):
        myDate = gtk.Dialog(_('Direct Debit savings at date'),
                            self.m_wMainWin,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            (gtk.STOCK_OK, gtk.RESPONSE_APPLY,
                             gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT))
        vbox = myDate.vbox
        date = gtk.Calendar()
        date.connect('day_selected',self.BudDaySelected_cb)
        vbox.add(date)
        myDate.show_all()
        response = myDate.run()
        self.getBudDay(date)
        self.m_wBudDDTargetDateEntry.set_text(self.m_sBudDay)
        sDDTarget = self.calculateDDTo(self.m_sBudDay)
        self.m_wBudTargetNeededEntry.set_text(sDDTarget)
        myDate.destroy()
        return

    def on_ChooseFirstDate_cb(self,me):
        response = gtk.RESPONSE_REJECT
        myDate = gtk.Dialog(_('First Payment Date'),
                            self.m_wMainWin,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            (gtk.STOCK_OK, gtk.RESPONSE_OK,
                             gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT))
        vbox = myDate.vbox
        date = gtk.Calendar()
        date.connect('day_selected',self.BudDaySelected_cb)
        vbox.add(date)
        myDate.show_all()
        response = myDate.run()
        if response == gtk.RESPONSE_OK:
            iyear,imon,iday = date.get_date()
            imon = imon +1
            self.m_sBudDay = "%d/%d/%d" % (iday,imon,iyear)
            mon = "%d" % imon
            sDate = "%d" % iday
            sDate += '/'
            sDate += mon
            self.m_wBudDateFirstEntry.set_text(sDate)
        myDate.destroy()
        return


    def budgetTreeSelection_cb(self,me):
        treePath,col = self.m_wBudgetTree.get_cursor()
        model = self.m_wBudgetTree.get_model()
        iter = model.get_iter(treePath)
        catMaj = model.get_value(iter,0)
        catMin = model.get_value(iter,1)
        sTotal = model.get_value(iter,2)
        sFreq = model.get_value(iter,3)
        sStartDate = model.get_value(iter,4)
        sDD = model.get_value(iter,5)
        self.m_wBudMajEntry.set_text(catMaj)
        self.m_wBudSubCatEntry.set_text(catMin)
        self.m_wBudYearlyEntry.set_text(addDollar(sTotal))
        iSpin = string.atoi(sFreq)
        self.m_wBudNumSchedSpin.set_value(iSpin)
        self.m_wBudDateFirstEntry.set_text(sStartDate)
        if(sDD != 'c') and (sDD != 'C'):
            self.m_wBudCashOrDDEntry.set_text('D')
        else:
            self.m_wBudCashOrDDEntry.set_text('C')
            self.m_LastBudgetRecord[0] = catMaj
            self.m_LastBudgetRecord[1] = catMin
            self.m_LastBudgetRecord[2] = sTotal
            self.m_LastBudgetRecord[3] = sFreq
            self.m_LastBudgetRecord[4] = sStartDate
            self.m_LastBudgetRecord[5] = sDD
        self.m_iBudEditCurrent = treePath[0]
        return

    def on_BudgetReplace_cb(self,me):
        catMaj = self.m_wBudMajEntry.get_text()
        catMin = self.m_wBudSubCatEntry.get_text()
        fTotal = dollarToFloat(self.m_wBudYearlyEntry.get_text())
        sTotal = floatNoDollar(fTotal)
        iSpin = self.m_wBudNumSchedSpin.get_value()
        sSpin = "%d" % iSpin
        sDateFirst = self.m_wBudDateFirstEntry.get_text()
        sCashOrDD = self.m_wBudCashOrDDEntry.get_text()
        sCashOrDD = string.lower(sCashOrDD)
        sCashOrDD = string.strip(sCashOrDD)
        if (sCashOrDD[0] == 'y') or (sCashOrDD[0] == 'c'):
            sCashOrDD = 'C'
        else:
            sCashorDD = 'D'
        newItem = ['','','','','','']
        newItem[0] = catMaj
        newItem[1] = catMin
        newItem[2] = addDollar(sTotal)
        newItem[3] = sSpin
        newItem[4] = sDateFirst
        newItem[5] = sCashOrDD
        if self.m_iBudEditCurrent >= 0:
            self.m_LastBudgetRecord = self.m_allItems[self.m_iBudEditCurrent] 
        if self.m_iBudEditCurrent >= 0:
            self.m_allItems[self.m_iBudEditCurrent] = newItem
        else:
            return
        self.rebuildTrees()
        self.updateRecordTree(newItem)
        self.setDirty()
        return

    def rebuildTrees(self):
        self.m_wCategoryTree.destroy()
        self.m_wBudgetTree.destroy()
        self.m_wResultsCategoryTree.destroy()
        self.m_wImportCategoryTree.destroy()
        self.m_allItems.sort()
        self.m_wBudgetTree = self.buildBudgetTree()
        self.m_wCategoryTree = self.rebuildCategoryTree()
        self.m_wImportCategoryTree = self.buildImportCategoryTree()
        self.m_wResultsCategoryTree = self.rebuildResultsCategoryTree()
        self.rebuildDDTree()
        sel = self.m_wCategoryTree.get_selection()
        sel.set_mode (gtk.SELECTION_BROWSE)
        sel.connect('changed',self.catTreeSelection_cb)
        sel2 = self.m_wResultsCategoryTree.get_selection()
        sel2.set_mode(gtk.SELECTION_MULTIPLE)
        sel2.connect('changed',self.onResultsCatSelected_cb)
        self.m_iResultsCatSelected = 0
        sel3 = self.m_wBudgetTree.get_selection()
        sel3.set_mode (gtk.SELECTION_BROWSE)
        sel3.connect('changed',self.budgetTreeSelection_cb)
        sel4 = self.m_wImportCategoryTree.get_selection()
        sel4.set_mode (gtk.SELECTION_BROWSE)
        sel4.connect('changed',self.catTreeImportSelection_cb)
        return

#
# Build the Import Tab now
#
#
# Initial building of the category tree
#
#
# The Category Tree in the Import window from a pre-existing
# self.m_allItems
#
    def buildImportCategoryTree(self):
        catmodel = self.buildCategoryModel()
        catwin = self.m_wImpCatWindow
        catTree = gtk.TreeView(catmodel)
        catTree.set_rules_hint(True)
        column = gtk.TreeViewColumn(_('Categories'), gtk.CellRendererText(),
                                    text=0)
        catTree.append_column(column)
        catwin.add(catTree)
        catwin.show_all()
        return catTree
#
# Selection callback for the Import catergory tree
#
    def catTreeImportSelection_cb(self,selection):
        selection = selection.get_selected()
        if not selection:
            return
        model, iter = selection
        item_str = model.get_value(iter,0)
        item_outer = model.get_value(iter,1)
        item_inner = model.get_value(iter,2)
        item_index = model.get_value(iter,3) -1
        self.m_iCurrentCatImportIndex = item_index
#    print 'selected item ',item_str,' outer index ',item_outer,' inner index ',item_inner,' Total index ',item_index
#    str = m_allItems[item_index][0] + ' - ' + m_allItems[item_index][1]
#    print ' selected cat is ',str
#    print 'Current Cat index is ',m_iCurrentCatIndex
        self.m_wImpCat1.set_text(self.m_allItems[item_index][0] )
        self.m_wImpCat2.set_text(self.m_allItems[item_index][1] )
        return
#
# Import Date callback
#
    def importDate_cb(self,me):
        year, month, day = self.m_wImpDateCal.get_date()
        str = "%d/%d/%d" % (day,month+1,year)
        self.m_wImpDateEntry.set_text(str)
#
# Create import model
#
    def createImportRecordsTree(self):
        self.m_iNumImportRecords = 0
        store = gtk.TreeStore(gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING,
                              gobject.TYPE_STRING)
        recwin = self.m_wImpImpWindow
        recTree = gtk.TreeView(store)
        recTree.set_rules_hint(True)
        sel = recTree.get_selection()
        sel.set_mode (gtk.SELECTION_MULTIPLE)
        column = gtk.TreeViewColumn(_('Account  '), gtk.CellRendererText(),
                                    text=0)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Sub Account  '), gtk.CellRendererText(),
                                    text=1)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Date '), gtk.CellRendererText(),
                                    text=2)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Amount '), gtk.CellRendererText(),
                                    text=3)
        recTree.append_column(column)
        column = gtk.TreeViewColumn(_('Comments '), gtk.CellRendererText(),
                                    text=4)
        recTree.append_column(column)
        recwin.add(recTree)
        recwin.show_all()
        TreeStore = recTree.get_model()
        if self.m_iHaveImportData == 1:
            j = 0
            totalNum = 0
            for frag in self.allImpRecords:
                if j == 0 or j == 1:
                    j = j + 1
                    continue
                f = dollarToFloat(frag[3])
                s = floatToStr(f)
                TreeStore.append(None,(frag[0],frag[1],frag[2],s,frag[4]))
                j = j +1
                totalNum = totalNum + 1
            self.m_iNumImportRecords = totalNum
            sel = recTree.get_selection()
            sel.set_mode (gtk.SELECTION_BROWSE)
            sel.connect('changed',self.ImportRecTree_cb)

        return recTree

# Callback when a user clicks on the import records tree

    def ImportRecTree_cb(self,me):
        treePath,Col = self.m_wImpRecordTree.get_cursor()
        if treePath == None:
            return
        treeModel = self.m_wImportRecTree.get_model()
        self.m_iEditImport = treePath[0]
        self.m_EditIterImport = treeModel.get_iter(treePath)
        valCatMajor = treeModel.get_value(self.m_EditIterImport,0)
        valCatMinor = treeModel.get_value(self.m_EditIterImport,1)
        valDateText = treeModel.get_value(self.m_EditIterImport,2)
        valAmountText = treeModel.get_value(self.m_EditIterImport,3)
        valComment = treeModel.get_value(self.m_EditIterImport,4)
        self.m_wImpCat1.set_text(valCatMajor)
        self.m_wImpCat2.set_text(valCatMinor)
        self.m_wImpDateEntry.set_text(valDateText)
        self.m_wImpAmount.set_text(valAmountText)
        myBuffer = gtk.TextBuffer()
        myBuffer.set_text(valComment)
        self.m_wImpDesc.set_buffer(myBuffer)
        self.m_wImpDesc.show_all()
        return

# Code to bring up a file chooser dialog and set things up for the data
# import

    def on_ImportDataButton_cb(self,me):
        print 'Import button clicked'
        self.m_sImportFile, self.m_iImportFileType = self.chooseImportFile()
        if self.m_iImportFileType == 1:
            self.allImpRecords = self.m_impGNUCash.importData(self.m_sImportFile)
            self.rebuildImportRecords()
        return

# OK rebuild the import record tree. Call this after a new set of import data is loaded

    def rebuildImportRecords(self):
        self.m_wImportRecTree.destroy()
        self.m_wImportRecTree = self.createImportRecordsTree()
        return
    
# Code to add current record to the full record tree.

    def on_ImportAddButton_cb(self,me):
        print 'Import Add button clicked'
        return

# Returns the file and the file type of importable data

    def chooseImportFile(self):
        response = gtk.RESPONSE_CANCEL
        wFileSel = gtk.FileSelection(_('Choose File for Import'))
        wFileSel.show_fileop_buttons()
        if self.m_sImportFile == '':
            self.m_sImportFile = self.m_RecentFiles.getNthRecent(1)
            self.m_sImportFile = self.getPathOnly(self.m_sImportFile)
        wFileSel.set_filename(self.m_sImportFile)
        response = wFileSel.run()
        if response == gtk.RESPONSE_CANCEL:
            sCurrentFile = ""
        else:
            sCurrentFile = wFileSel.get_filename()
        wFileSel.destroy()
        iFType = self.sniffFile(sCurrentFile)
        print 'Result of sniff ',iFType
        return sCurrentFile,iFType

# Code to delete the current record from the.

    def on_ImportDeleteButton_cb(self,me):
        print 'Import Delete button clicked'
        return

    def sniffFile(self,sImportFile):
        iFType = self.m_impGNUCash.sniff(sImportFile)
        if iFType != 0:
            return iFType
        return 0
    
# end of import TAB

    def on_BudgetSort_cb(self,me):
        self.setDirty()
        self.rebuildTrees()
        return

    def on_BudgetAdd_cb(self,me):
        catMaj = self.m_wBudMajEntry.get_text()
        catMin = self.m_wBudSubCatEntry.get_text()
        if self.validateNum(self.m_wBudYearlyEntry.get_text()) == 0:
            self.errorNumber(self.m_wBudYearlyEntry.get_text())
            return
        fTotal = dollarToFloat(self.m_wBudYearlyEntry.get_text())
        sTotal = floatNoDollar(fTotal)
        iSpin = self.m_wBudNumSchedSpin.get_value()
        sSpin = "%d" % iSpin
        sDateFirst = self.m_wBudDateFirstEntry.get_text()
        if self.validateMonthDate(sDateFirst) == 0:
            self.errorDate(sDateFirst)
            return
        sCashOrDD = self.m_wBudCashOrDDEntry.get_text()
        sCashOrDD = string.lower(sCashOrDD)
        sCashOrDD = string.strip(sCashOrDD)
        if (sCashOrDD[0] == 'y') or (sCashOrDD[0] == 'c'):
            sCashOrDD = 'C'
        else:
            sCashorDD = 'D'
        newItem = ['','','','','','']
        newItem[0] = catMaj
        newItem[1] = catMin
        newItem[2] = addDollar(sTotal)
        newItem[3] = sSpin
        newItem[4] = sDateFirst
        newItem[5] = sCashOrDD
        self.m_LastBudgetRecord = newItem
        self.m_allItems.append(newItem)
        self.rebuildTrees()
        self.updateRecordTree(newItem)
        self.setDirty()

# Fixme should search for this item

        self.m_iBudEditCurrent = -1
        return

    def on_BudgetDelete_cb(self,me):
        treePath,col = self.m_wBudgetTree.get_cursor()
        model = self.m_wBudgetTree.get_model()
#
# Should pop-up a nice help window
#
        if treePath == None:
            return
    
        iter = model.get_iter(treePath)
        self.m_iBudEditCurrent = treePath[0]
#
# FIXME should handle the records too
#
        del self.m_allItems[self.m_iBudEditCurrent]
        self.rebuildTrees()
        self.setDirty()
        self.m_iBudEditCurrent = -1
        return

    def on_BudgetNew_cb(self,me):
        self.m_iBudEditCurrent = -1
        self.m_wBudMajEntry.set_text('')
        self.m_wBudSubCatEntry.set_text('')
        self.m_wBudYearlyEntry.set_text('')
        iSpin = string.atoi('1')
        self.m_wBudNumSchedSpin.set_value(iSpin)
        self.m_wBudDateFirstEntry.set_text('0/0')
        self.m_wBudCashOrDDEntry.set_text(_('yes'))
        return

    def getPathOnly(self,sFileName):
        ilen = len(sFileName)
        bstop = 1
        j = ilen -1
        while (j>=0) and (bstop == 1):
            if sFileName[j] == self.m_pDelim:
                bstop = 0
            else:
                j = j -1
        if j < 0:
            return ''
        else:
            return sFileName[0:j+1]
    
    def onGnumericExportRecords(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'records.gnumeric'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportGnumeric.export(self.m_wRecordTree,sFile,self.m_sRecordHeadings)
        return

    def onGnumericExportResults(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'results.gnumeric'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportGnumeric.export(self.m_wResultsTree,sFile,self.m_sResultsHeadings)
        return

    def onGnumericExportBudget(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'budget.gnumeric'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportGnumeric.export(self.m_wBudgetTree,sFile,self.m_sBudgetHeadings)
        return


    def onGnumericExportDD(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'DirectDebit.gnumeric'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportGnumeric.export(self.m_wDDResultsTree,sFile,self.m_sDDHeadings)
        return

    
    def onAbiExportRecords(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'records.abw'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportAbiWord.export(self.m_wRecordTree,sFile,self.m_sRecordHeadings)
        return

    def onAbiExportResults(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'results.abw'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportAbiWord.export(self.m_wResultsTree,sFile,self.m_sResultsHeadings)
        return

    def onAbiExportBudget(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'budget.abw'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportAbiWord.export(self.m_wBudgetTree,sFile,self.m_sBudgetHeadings)
        return


    def onAbiExportDD(self,me):
        self.m_sCurrentFile = self.m_RecentFiles.getNthRecent(1)
        spath = self.getPathOnly(self.m_sCurrentFile)
        sFile = spath + 'DirectDebit.abw'
        sFile = self.chooseExportFile(sFile)
        self.m_ExportAbiWord.export(self.m_wDDResultsTree,sFile,self.m_sDDHeadings)
        return

    def onConfigure_cb(self,me,event):
        (iWidth,iHeight) = self.m_wMainWin.get_size()
        sWidth = "%d" % iWidth
        sHeight = "%d" % iHeight
        if not iWidth==self.m_iWinWidth or not iHeight==self.m_iWinHeight:
            self.m_xpconf.setValue('width',sWidth)
            self.m_xpconf.setValue('height',sHeight)
            self.m_iWinHeight=iHeight
            self.m_iWinWidth=iWidth
        return

    def onHelp_cb(self,me):
        sDocPath = self.buildPath('docs'+self.m_pDelim+'Divifund.html')
        if self.m_OS.isWin():
            webbrowser.open(sDocPath)
        self.m_sBrowser = findLinuxBrowser(self.m_pDelim)
        if self.m_sBrowser != None:
            os.system(self.m_sBrowser+' '+sDocPath+' &')
        return

    def onAbout_cb(self,me):
        response = gtk.RESPONSE_OK
        qAbout = gtk.Dialog(_('About Divifund'),
                            self.m_wMainWin,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            (gtk.STOCK_OK, gtk.RESPONSE_OK))
        vbox = qAbout.vbox
        label = gtk.Label(_('Divifund is a personal finance program. \n (C) Martin Sevior <msevior@physics.unimelb.edu.au> \n This program is Free Software licensed under the Lesser General Public License (LGPL) \n'))
        vbox.add(label)
        qAbout.show_all()
        response = qAbout.run()
        qAbout.destroy()
        return

    def onExportAbi_cb(self,me):
        iPage = self.m_wNoteBook.get_current_page()
        if iPage == self.defEnterOrEdit:
            self.onAbiExportRecords(me)
        elif iPage == self.defTotals:
            self.onAbiExportResults(me)
        elif iPage == self.defBudget:
            self.onAbiExportBudget(me)
        elif iPage == self.defDirectDebit:
            self.onAbiExportDD(me)
        return


    def onExportGnumeric_cb(self,me):
        iPage = self.m_wNoteBook.get_current_page()
        if iPage == self.defEnterOrEdit:
            self.onGnumericExportRecords(me)
        elif iPage == self.defTotals:
            self.onGnumericExportResults(me)
        elif iPage == self.defBudget:
            self.onGnumericExportBudget(me)
        elif iPage == self.defDirectDebit:
            self.onGnumericExportDD(me)
        return

    def on_Example_cb(self,me):
        sExample = buildPath('example.myb')
 #    print ' Open button pressed '
        if self.m_iDirty > 0:
            res = querySave()
            if res == gtk.RESPONSE_REJECT:
                return
        self.doOpenFile(sExample)
        return
#
# Connect some signals
#
    def connectSignals(self):
        sel = self.m_wCategoryTree.get_selection()
        sel.set_mode (gtk.SELECTION_BROWSE)
        sel.connect('changed',self.catTreeSelection_cb)
        sel2 = self.m_wResultsCategoryTree.get_selection()
        sel2.set_mode(gtk.SELECTION_MULTIPLE)
        sel2.connect('changed',self.onResultsCatSelected_cb)
        self.m_iResultsCatSelected = 0
        sel3 = self.m_wBudgetTree.get_selection()
        sel3.set_mode (gtk.SELECTION_BROWSE)
        sel3.connect('changed',self.budgetTreeSelection_cb)
        sel4 = self.m_wRecordTree.get_selection()
        sel4.set_mode (gtk.SELECTION_BROWSE)
        sel4.connect('changed',self.recordTreeSelection_cb)
        sel5 = self.m_wImportCategoryTree.get_selection()
        sel5.set_mode (gtk.SELECTION_BROWSE)
        sel5.connect('changed',self.catTreeImportSelection_cb)
    
        wMenuQuit = self.m_pXML.get_widget('wMenuQuit')
        wMenuQuit.connect('activate',self.saveAndQuit_cb)
        self.m_wDateSelector.connect('day_selected',self.daySelected_cb)
        self.m_wMainWin.connect('destroy', lambda win: self.SaveAndQuit())
        self.m_wMainWin.connect('configure-event',self.onConfigure_cb)
        self.m_wAddButton.connect('clicked',self.on_AddButton_cb)
        self.m_wTBSaveButton.connect('clicked',self.on_Save_cb)
        self.m_wMenuSave.connect('activate',self.on_Save_cb)
        self.m_wMenuSaveAs.connect('activate',self.on_SaveAs_cb)
        self.m_wTBNew.connect('clicked',self.on_New_cb)
        self.m_wMenuNew.connect('activate',self.on_New_cb)
        self.m_wTBOpen.connect('clicked',self.on_Open_cb)
        self.m_wMenuOpen.connect('activate',self.on_Open_cb)
        self.m_wMenuExample.connect('activate',self.on_Example_cb)
        self.m_wMainReplace.connect('clicked',self.on_MainReplace_cb)
        self.m_wMainDelete.connect('clicked',self.on_MainDelete_cb)
        self.m_wResultsDateLow.connect('day_selected',self.on_ResultLow_cb)
        self.m_wResultsDateHigh.connect('day_selected',self.on_ResultHigh_cb)
        self.m_wResultsApply.connect('clicked',self.on_ApplyResultsRange_cb)
        self.m_wResultsAllCategories.connect('clicked',self.onResultsAllCats_cb)
        self.m_wMainSort.connect('clicked',self.on_MainSort_cb)
        self.m_wSortDates.connect('clicked',self.on_MainSortDates_cb)
        self.m_wBudChooseDDTargetButton.connect('clicked',self.on_ChooseDDTarget_cb)
        self.m_wBudChooseFirstDateButton.connect('clicked',self.on_ChooseFirstDate_cb)
        self.m_wBudReplace.connect('clicked',self.on_BudgetReplace_cb)
        self.m_wBudSort.connect('clicked',self.on_BudgetSort_cb)
        self.m_wBudAdd.connect('clicked',self.on_BudgetAdd_cb)
        self.m_wBudDelete.connect('clicked',self.on_BudgetDelete_cb)
        self.m_wBudNew.connect('clicked',self.on_BudgetNew_cb)
        self.m_wMenuAbout.connect('activate',self.onAbout_cb)
        self.m_wMenuHelp.connect('activate',self.onHelp_cb)
        self.m_wTBExportAbi.connect('clicked',self.onExportAbi_cb)
        self.m_wTBExportGnumeric.connect('clicked',self.onExportGnumeric_cb)
        self.m_wMenuExportAbi.connect('activate',self.onExportAbi_cb)
        self.m_wMenuExportGnumeric.connect('activate',self.onExportGnumeric_cb)
        self.m_wDDCalendar.connect('day-selected',self.onDDDateChanged_cb)
        self.m_wImpDateCal.connect('day-selected',self.importDate_cb)
        self.m_wImportDataButton.connect('clicked',self.on_ImportDataButton_cb)
        self.m_wImportAddButton.connect('clicked',self.on_ImportAddButton_cb)
        self.m_wImportDeleteButton.connect('clicked',self.on_ImportDeleteButton_cb)

    def main(self):
        sWidth = self.m_xpconf.readValue('width')
        sHeight = self.m_xpconf.readValue('height')
        self.connectSignals()
        if (sWidth == None) or (sHeight == None):
            self.m_wMainWin.show_all()
            (iWidth,iHeight) = self.m_wMainWin.get_size()
            sWidth = "%d" % iWidth
            sHeight = "%d" % iHeight
            self.m_xpconf.setValue('width',sWidth)
            self.m_xpconf.setValue('height',sHeight)
        else:
            iWidth = string.atoi(sWidth)
            iHeight = string.atoi(sHeight)
            self.m_wMainWin.resize(iWidth,iHeight)
            self.m_wMainWin.show_all()
        self.m_iWinWidth=iWidth
        self.m_iWinHeight=iHeight
        gtk.main()

	   

