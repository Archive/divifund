# XPTools Cross Platform Utility Module for Python (Cross-Platform)
# Copyright 2004, Ryan Pavlik
# NO WARRANTY

import sys
import os
import string

#
# Cross Platform OS Detection Class
#

class XP_Detect:
    global m_pDelim
    def __init__(self):
        global m_isWin
        testpath = '/dev'
        self.m_isWin = 0
        self.m_pDelim = '/'
        if not os.path.isdir(testpath):
            self.m_pDelim = '\\'
            self.m_isWin = 1

    def getDelim(self):
        return self.m_pDelim

    def isWin(self):
        return self.m_isWin

class XP_Path:
    
    #
    # Checks if each directory in the path passed exists,
    # and creates it if it doesn't.
    #
    
    def checkPath(self,path):
        for sLetter in range(len(path)):
            if path[sLetter]=="/" or path[sLetter]=="\\":
                if not os.path.exists(path[:sLetter+1]):
                    os.mkdir(path[:sLetter+1])
                    print "XP_Path: Making path: " + path[:sLetter+1]
