#
# Neoconf Configuration Scheme Backend for:
# GConf 
# Available on:
# GNOME (Linux and compatible)
#

#
# Provides object-based wrapper for use by Neoconf.
# Neoconf is copyright 2004 by Ryan Pavlik,
# and is provided without warranty.
#

#
# This wrapper is copyright 2004 by Ryan Pavlik,
# and is provided without warranty.
#

import sys
import os
import string
import xptools

#
# Configure
#
# Set to 1 to display debug messages
confDebug = 1
#
#



def isAvailable():
    try:
        #
        # Module loading attempt here
        #
        import gconf
        have=1
    except:
        have=0
    return have  

class wrapper:
    def __init__(self):

        # Initialization:
        # Get the default client
        import gconf
        self.m_GConfClient = gconf.client_get_default()
        if confDebug:
            print 'neoconf: cs_gconf: Backend initialized and instantiated.'

    def setValue(self,key,value):
        #
        # Set value of key at path
        # Path delim is passed as forward slash.
        #
        #NOTE: I really should change the delims, but I'll leave that for later.
        skey = '/app/'+key
        self.m_GConfClient.set_string( skey,value)
        if confDebug:
            print 'neoconf: cs_gconf: Value set - ' +skey+"="+value

    def readValue(self,key):
        #
        # Read value of key at path
        # Path delim is passed as forward slash, convert if needed.
        #
        sKey = '/app/'+key
        sValue = self.m_GConfClient.get_string(sKey)
        if confDebug :
            print 'neoconf: cs_gconf: key= ',sKey,' Value=',sValue
        return sValue
            
            
