#
# Neoconf Configuration Scheme Backend for:
# FSConfTree 
# Available on:
# All platforms
#

#
# Provides object-based wrapper for use by Neoconf.
# Neoconf is copyright 2004 by Ryan Pavlik,
# and is provided without warranty.
#

#
# This wrapper is copyright 2004 by Ryan Pavlik,
# and is provided without warranty.
#

#
# Use this wrapper as a template for new backends
# for the Neoconf system.
#
import sys
import os
import string
import xptools

#
# Configure
#
# Set to 1 to display debug messages
confDebug = 1
#
#

#
global m_OS
global m_Path
m_homeDir = os.path.expanduser
m_OS=xptools.XP_Detect()
m_Path=xptools.XP_Path()
#

def isAvailable():
    try:
        #
        # Place module loading attempt here
        #
        return 1
    except:
        return 0
    
class wrapper:
    m_homeDir = os.path.expanduser
    def __init__(self):
        
        # Initialization: Check for presence of directories.
        # If not present, create.
        sPath=m_homeDir("~"+m_OS.getDelim()+'.fsconf')
        m_Path.checkPath(sPath)
        
        if confDebug:
            print 'neoconf: cs_fsconf initialized, instantiated'
        
        return

    def setValue(self,key,value):
        #
        # Set value of key at path
        # Path delim is passed as forward slash, convert if needed.
        #
        #NOTE: I really should change the delims, but I'll leave that for later.
        sPath=m_homeDir("~"+m_OS.getDelim()+'.fsconf'+m_OS.getDelim()+key)
        self.m_node=open(sPath, 'w')
        self.m_node.write(value)
        self.m_node.close()
        if confDebug:
            print 'neoconf: cs_fsconf: File written - ' +sPath
    def readValue(self,key):
        #
        # Read value of key at path
        # Path delim is passed as forward slash, convert if needed.
        #
        #NOTE: I really should change the delims, but I'll leave that for later.
        sPath=m_homeDir("~"+m_OS.getDelim()+'.fsconf'+m_OS.getDelim()+key)
        # Step through the path, building directories as we go.
        m_Path.checkPath(sPath)
        
        # Does the key exist?
        if not os.path.isfile(sPath):
            # If not, return none as a value.
            if confDebug:
                print 'neoconf: cs_fsconf: New key, no value'
            return None
        else:
            # If the node exists, read the first line, and return that
            # as the value.
            sNode=open(sPath, 'r')
            sValue=sNode.readline()
            sNode.close()
            if confDebug:
                print 'neoconf: cs_fsconf: File read - ' +sPath
                print 'neoconf: cs_fsconf: Value=' + sValue
            if sValue=='':
                if confDebug:
                    print 'neoconf: cs_fsconf: Empty Value, returning None'
                return None
                
            else:
                return sValue
            
            
