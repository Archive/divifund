#
# Neoconf Middle Layer
# Provides detection and abstraction for the
# following configuration schemes:
# FSConfTree
# GConf
#

#
# Provides scheme-agnostic wrapper for use by
# Neoconf High Level Services.
# Neoconf is copyright 2004 by Ryan Pavlik,
# and is provided without warranty.
#

import sys
import os
import string
import xptools

# Set to 1 to display debug messages
confDebug = 1

class midSettings:
    def __init__(self):

        #
        # For each scheme, provide the following lines
        #

        # FSConfTree
        import cs_fsconf
        self.m_hasFsconf=cs_fsconf.isAvailable()

        # GConf
        import cs_gconf
        self.m_hasGconf=cs_gconf.isAvailable()
        #self.m_hasGconf=0
        
        # Flag
        self.m_hasScheme=0
        # Do not edit
        
        #
        # Order of preference is set here, and the chosen backend
        # instantiated.
        # One group of lines is needed for each scheme.
        # Arrange in order of preference.
        #

        # GConf
        if self.m_hasGconf==1 and self.m_hasScheme==0:
            self.m_Backend=cs_gconf.wrapper()
            self.m_hasScheme=1
            if confDebug:
                print "neoconf: cs_middle: Backend chosen: GConf"

        # FSConfTree
        if self.m_hasFsconf==1 and self.m_hasScheme==0:
            self.m_Backend=cs_fsconf.wrapper()
            self.m_hasScheme=1
            if confDebug:
                print "neoconf: cs_middle: Backend chosen: FSConfTree"

        # Make sure scheme has been loaded, instantiated, and mapped.
        if self.m_hasScheme==0:
            print "neoconf: cs_middle: ERROR: No scheme available!"
            
        
        return

    def setValue(self,key,value):
        return self.m_Backend.setValue(key, value)

    def readValue(self,key):
        return self.m_Backend.readValue(key)
    
    
            
