#
#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

import sys
import os
import string

#
# Convert a string with an optional leading '$" o a floating point number
#
def dollarToFloat(str):
    s = string.strip(str)
    if s[0] == '$':
        ss = s[1:]
    else:
        ss = s
    fl = float(ss)
    return fl
   
#
# Trim leading trailing spaces
#
def trimString(s) :
    i = 0
    len_s = len(s)
    if len_s == 0:
        return s
    j = len_s-1
    while (s[i] == ' ') and (i < len_s-1):
        i = i +1
    while ((s[j]== ' ') or (s[j] == '\n')) and (j >= 0) :
        j = j - 1
    if(j<0):
        return s[0:0]
    return s[i:j+1]

#
# Convert a floating point number to a string with a leading dollar sign
#
def floatToStr(f) :
    iV = 0
    fV = f*100.0+0.499999
    iV = fV
    ff = iV
    ff = ff/100.0
    str = "%10.2f" % ff
    str = trimString(str)
    str = '$'+str
    return str

def floatNoDollar(f):
    iV = 0
    fV = f*100.0+0.499999
    iV = fV
    ff = iV
    ff = ff/100.0
    str = "%10.2f" % ff
    str = trimString(str)
    return str

def addDollar(str):
    s = trimString(str)
    i = 0
    len_s = len(s)
    if len_s == 0:
        return s
    if s[0] != '$' :
        ss = '$' + s
    else:
        ss = s
    return ss

    
sPath = os.getenv('PATH')
sPaths = string.split(sPath,':')

def findLinuxBrowser(sDelim):
    global sPaths
#
# epiphany
#
    for item in sPaths:
        sweb = item+sDelim+'epiphany'
        if os.path.isfile(sweb):
            return 'epiphany'
#
# firefox
#
    for item in sPaths:
        sweb = item+sDelim+'firefox'
        if os.path.isfile(sweb):
            return 'firefox'
#
# galeon
#
    for item in sPaths:
        sweb = item+sDelim+'galeon'
        if os.path.isfile(sweb):
            return 'galeon'
#
# mozilla
#
    for item in sPaths:
        sweb = item+sDelim+'mozilla'
        if os.path.isfile(sweb):
            return 'mozilla'
#
# konqueror
#
    for item in sPaths:
        sweb = item+sDelim+'konqueror'
        if os.path.isfile(sweb):
            return 'konqueror'
#
# netscape
#
    for item in sPaths:
        sweb = item+sDelim+'netscape'
        if os.path.isfile(sweb):
            return 'netscape'
    return None
