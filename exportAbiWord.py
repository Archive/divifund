#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


import sys
import os
import string

import xpconf
import xptools

m_OS=xptools.XP_Detect()
m_pDelim = m_OS.getDelim()

#
# Some useful code that should be available in all modules
#
from utilTools import *

class ExportAbiWord:
    def __init__(self):
        self.m_sHead = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE abiword PUBLIC "-//ABISOURCE//DTD AWML 1.0 Strict//EN" "http://www.abisource.com/awml.dtd">
<abiword template="false" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:svg="http://www.w3.org/2000/svg" xmlns:dc="http://purl.org/dc/elements/1.1/" fileformat="1.1" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:awml="http://www.abisource.com/awml.dtd" xmlns="http://www.abisource.com/awml.dtd" xmlns:xlink="http://www.w3.org/1999/xlink" version="2.1.3" xml:space="preserve" props="dom-dir:ltr; document-footnote-restart-section:1; document-endnote-type:numeric; document-endnote-place-enddoc:1; document-endnote-initial:1; lang:en-US; document-endnote-restart-section:1; document-footnote-restart-page:1; document-footnote-type:numeric; document-footnote-initial:1; document-endnote-place-endsection:1">
<!-- ======================================================================== -->
<!-- This file is an AbiWord document.                                        -->
<!-- AbiWord is a free, Open Source word processor.                           -->
<!-- More information about AbiWord is available at http://www.abisource.com/ -->
<!-- You should not edit this file by hand.                                   -->
<!-- ======================================================================== -->

<metadata>
<m key="dc.format">application/x-abiword</m>
<m key="abiword.generator">AbiWord</m>
</metadata>
<history version="2" edit-time="118" last-saved="1086776048" uid="debdd8ae-b9fc-11d8-9833-e93c37a47d37">
<version id="1" started="1086775769" uid="1834c2e6-b9fd-11d8-9833-e93c37a47d37" auto="0"/>
<version id="2" started="1086776048" uid="bea12fe8-b9fd-11d8-86e7-824f8e44157b" auto="0"/>
</history>
<styles>
<s type="P" name="Normal" followedby="Current Settings" props="font-family:Times New Roman; dom-dir:ltr; color:000000; margin-left:0pt; text-position:normal; widows:2; font-style:normal; text-indent:0in; font-variant:normal; margin-top:0pt; font-weight:normal; margin-right:0pt; font-size:12pt; text-decoration:none; margin-bottom:0pt; line-height:1.0; bgcolor:transparent; text-align:left; font-stretch:normal"/>
</styles>
<pagesize pagetype="A4" orientation="portrait" width="210.000000" height="297.000000" units="mm" page-scale="1.000000"/>
<section>
<p><c></c></p>
"""
        self.sPath = os.getenv('PATH')
        self.sPaths = string.split(sPath,':')


    def removeAmps(self,str):
        ilen = len(str)
        j = 0
        ostr = ''
        while j< ilen :
            if str[j] == '&':
                ostr = ostr + 'and'
            else:
                ostr = ostr + str[j]
            j = j + 1
        return ostr

    def findAbiWord(self):
        for item in self.sPaths:
            abi = item+m_pDelim+'AbiWord-2.2'
            if os.path.isfile(abi):
                return 'AbiWord-2.2'
        for item in self.sPaths:
            abi = item+m_pDelim+'abiword-2.2'
            if os.path.isfile(abi):
                return 'abiword-2.2'
        for item in self.sPaths:
            abi = item+m_pDelim+'abiword-2.0'
            if os.path.isfile(abi):
                return 'abiword-2.0'
        return None

#
# Export a gtk tree to an AbiWord file
#
    def export(self,tree,sAbiFile,sHeadings):
        fAbiFileName = open(sAbiFile,'w')

        model = tree.get_model()
        iterFirst = model.get_iter_first()
        countRows = 0
        iter = iterFirst
        bstop = 1
        while bstop==1:
            iter = model.iter_next(iter)
            if iter != None:
                countRows = countRows + 1
            else:
                bstop = 0
        numCols = 0
        bStop = 1
        curCol = tree.get_column(numCols)
        if curCol == None:
            bStop =0
        if bStop == 1:
            colwidth0 = curCol.get_width()    
            numCols = numCols +1
            colWidths = [colwidth0]
            while bStop == 1:
                curCol =  tree.get_column(numCols)
                if curCol == None:
                    bStop =0
                else:
                    width = tree.get_column(numCols).get_width()
                    if width > 200 :
                        width = 200
                    colWidths.append(width)
                    numCols = numCols +1
#
# write out the boiler plate header for a simple AbiWord file
#
        for lin in self.m_sHead:
            fAbiFileName.write(lin)

# Table header with widths set to reasonable values
#
#    lin = '<table props="table-column-props:'
#    colwidth = 0.0
#    swidth = ''
#    for i in range(numCols):
#        colwidth = colWidths[i]
#        val = colwidth*25.4/86.0
#        swidth = '%f' % val
#        lin += swidth
#        lin += 'mm/'
#    lin +='">'
#    print lin
#
# Don't set fix width coz it might flow past the edge of abiword.
# Instead let the user worry about it.
        lin = '<table>'
        fAbiFileName.write(lin)

# OK write out the table

        iter = iterFirst
        bstop = 1
        row = 0
        bDoHeadings = 0
        if sHeadings != None:
            bDoHeadings = 1
        while bstop ==1:
            for i in range(numCols):
                sval = model.get_value(iter,i)
                sval = trimString(sval)
                sval = self.removeAmps(sval)
                fval = 0.0
                isnum = 0
                iright = i+1
                ibot = row +1
                sbot = '%d' % ibot
                sleft = '%d' % i
                sright = '%d' % iright
                sstop = '%d' % row
                lin = '<cell props="bot-attach:'+sbot+'; left-attach:'+sleft+'; right-attach:'+sright+'; top-attach:'+sstop+'">'
#            print lin
                fAbiFileName.write(lin)
                if (len(sval)> 0) and (sval[0] == '$'):
                    fval = dollarToFloat(sval)
                    isnum = 1
                elif (len(sval)>0) and sval.isdigit():
                    fval = string.atof(sval)
                    isnum = 1
                if bDoHeadings == 1:
                    sval = sHeadings[i]
                if isnum == 0 and len(sval) == 0:
                    lin = '<p style="Normal"></p>'
                else:
                    lin = '<p style="Normal">%s</p>' % sval
                fAbiFileName.write(lin)
                lin = '</cell>'
                fAbiFileName.write(lin)
            if bDoHeadings == 1:
                iter = iterFirst
            else:
                iter = model.iter_next(iter)
            bDoHeadings = 0
            if iter != None:
                row = row + 1
            else:
                bstop = 0
        fAbiFileName.write('</table>')
        fAbiFileName.write('<p></p>')
        fAbiFileName.write('</section>')
        fAbiFileName.write('</abiword>')
        fAbiFileName.close()
#
# OK now start up a AbiWord process with this file
#
        sAbiExec = self.findAbiWord()
        if sAbiExec != None:
            lin = sAbiExec+' '+sAbiFile+' &'
            os.system(lin)
        else:
            response = gtk.RESPONSE_NO
            noAbi = gtk.Dialog(_('No AbiWord',
                             m_wMainWin,
                             gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                             (gtk.STOCK_OK, gtk.RESPONSE_NO)))
            vbox = noAbi.vbox
            label = gtk.Label(_('You do not have the AbiWord Word processor installed on your system. \n You can download this excellent and free Word Processor from http://www.abisource.com'))
            vbox.add(label)
            noAbi.show_all()
            response = noAbi.run()
            noAbi.destroy()
        return
