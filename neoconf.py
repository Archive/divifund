import sys
import os
import string
import xptools
import cs_middle

# Set to 1 to display debug messages
confDebug = 1

global m_OS
m_homeDir = os.path.expanduser
m_OS=xptools.XP_Detect()

class settings:
    # Set Application Key (name, no spaces) for use across all platforms
    global appKey
    
    def __init__(self, appName):
        self.appKey=appName
        if confDebug:
            print "neoconf settings initializing:" + self.appKey

        self.m_Scheme=cs_middle.midSettings()
        return
    
