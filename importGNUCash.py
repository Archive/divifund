#
# Copyright (C) Martin Sevior, <msevior@physics.unimelb.edu.au> 2004
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with the program; see the file LICENSE.  If not,
# write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


import sys
import os
import string

import xpconf
import xptools
import xml.parsers.expat

m_OS=xptools.XP_Detect()
m_pDelim = m_OS.getDelim()

#
# Some useful code that should be available in all modules
#
from utilTools import *

class ImportGNUCash:
    def __init__(self):
        self.sPath = os.getenv('PATH')
        self.sPaths = string.split(sPath,':')

# Returns 1 if file is recognized as a GNUCash file

    def sniff(self,sImportFile):
        fSniff = open(sImportFile)
        l1 = fSniff.readline()
        v1 = """<?xml version="1.0"?>
"""
        v2 = """<gnc-v2>
"""
        if l1 != v1:
            fSniff.close()
            return 0
        l2 = fSniff.readline()
        fSniff.close()
        if l2 == v2:
            return 1
        return 0

# returns a 2-D tuple of strings
# m_ImpRecords[i][0] == 'Account Name'
# m_ImpRecords[i][1] == 'Sub Account Name'
# m_ImpRecords[i][2] == 'Date of transaction'
# m_ImpRecords[i][3] == 'Amount of Transaction'
# m_ImpRecords[i][4] == 'Any other info about the transaction'


    def importData(self,sImportFile):
        self.m_ImpRecords = [['','','','',''],['','','','','']]
        self.m_iNumRecords = 0
        self.m_MainAccounts = {'dum1':'aaaaaaaaaaaaaaaaaa'}
        self.m_SubAccounts = {'dum2':'aaaaaaaaaaaaaaaaaa'}
#
# Need to know the state of import
#
# Interesting  states are:
# 0 not useful right now
# 1 Account definition
# 2 Transaction definition
#
        self.m_State = 0
#
# The substate is the state of the Account definition or transaction definition
# Interesting avlues are:
# 0 Just entered main state
# 1 Expecting character data
# other sub states will be defined and used as needed
#
        self.m_SubState = 0
        self.m_Date = ' '
        self.m_Desc = ' '
#        self.m_iParseStack = self.ParseStack()
#        self.m_XMLImport =
        self.m_ExpatParser = xml.parsers.expat.ParserCreate()
        self.m_ExpatParser.StartElementHandler = self.startElement
        self.m_ExpatParser.EndElementHandler = self.endElement
        self.m_ExpatParser.CharacterDataHandler = self.charElement
        fImport = open(sImportFile)
        rImport = fImport.read()
        self.m_ExpatParser.Parse(rImport)
        fImport.close()
        return self.m_ImpRecords

    def startElement(self,name,attrs):
        print 'Start Element ', name,attrs
        
    def endElement(self,name):
        print 'End Element: ',name

    def charElement(self,data):
        print ' Character data: ',repr(data)
        
