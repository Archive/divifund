import sys
import os
import string
import neoconf
import cs_middle

print "Testing neoconf"
settings=neoconf.settings("test")
print "Test completed: instantiation complete"

print "Testing cs_middle"
middle=cs_middle.midSettings()
print "Instantiation complete"

print "Reading Value"
value=middle.readValue("test/test")
print "Printing value"
print value

print "Setting value"
middle.setValue("test/test","pizza")
print "Value set"

print "Reading value"
newvalue=middle.readValue("test/test")

print "Printing value"
print newvalue

print "Comparing"
if newvalue==value:
    print "Compare passed: roundtrip of value complete"
else:
    print "Compare failed: value did not roundtrip"
    
print "Overwriting value"
middle.setValue("test/test","elephant")
print "New value written"

value3=middle.readValue("test/test")
print "New value read, returned"
print value3


